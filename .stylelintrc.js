module.exports = {
    extends: 'stylelint-config-recommended',
    rules: {
        'selector-pseudo-class-no-unknown': [
            true,
            { ignorePseudoClasses: ['local', 'global'] },
        ],
        'at-rule-no-unknown': [true, {ignoreAtRules: ['include']}],
        'property-no-unknown': [true, { ignoreProperties: ['composes'] }],
    },
};
