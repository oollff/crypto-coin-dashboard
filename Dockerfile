FROM node:9-alpine
WORKDIR /app
COPY package.json .npmrc ./
COPY dist /app/dist
# --no-optional: Stänger av varningar, --only=production: Bara dependencies som behövs
RUN npm install --no-optional --only=production
#[] används för att ctrl-c ska fungera: https://forums.docker.com/t/docker-run-cannot-be-killed-with-ctrl-c/13108
CMD ["npm", "run", "prod"]