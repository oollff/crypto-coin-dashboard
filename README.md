# CI

## Build stage
Whenever a commit gets pushed to a branch, Gitlab CI will trigger a build job where
a docker image is built and saved as a downloadable `.tar` file.
The saved `.tar` file is called `crypto-dashboard:$CI_PIPELINE_ID` and is located
in a `tmp` folder.

*The downloadable file will expire after 3 hours, i.e. will not be downloadable or used in 
subsequent deploy stage.*

## Deploy stage
*This stage is manually triggered only on master branch.*

Given a successful build stage, this stage will do the following:
1. Copy the Docker `.tar` file deriving from the build stage to the production server
2. Log in to the server

 > *Server context*

1. Load .tar file as Docker image
2. Stop currently running container
3. Start new Docker container based on new image

*Note: containers are not removed automatically. This allows for easy rollback to a previously deployed
version of the app if needed.*
