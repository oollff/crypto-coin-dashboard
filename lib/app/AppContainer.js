import React, { PureComponent } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { Switch, withRouter } from 'react-router-dom';
import routes from 'routes/routes';
import Header from 'pageLayout/header/Header';
import { routeWithSubRoutes } from 'shared/utils/routerUtils/routerUtils';
import { auth } from 'connectivity/api.firebase';
import { setAuthUser } from 'shared/reduxStore/sessionState';

class AppContainer extends PureComponent {
  componentDidMount() {
    const { setAuthUser } = this.props;

    auth.onAuthStateChanged(authUser => {
      authUser
        ? setAuthUser(authUser)
        : setAuthUser(null);
    });
  }

  render() {
    return (
      <div id="app-container">
        <Helmet title="Crypto Dashboard" />
        <Header />
        <Switch>
          {routes.map((route, index) => routeWithSubRoutes(route, index))}
        </Switch>
      </div>
    );
  }
}

export default withRouter(connect(null, { setAuthUser })(AppContainer));
