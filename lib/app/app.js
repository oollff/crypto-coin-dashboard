import React from 'react';
import history from 'shared/utils/history';
import { Provider } from 'react-redux';
import Immutable from 'immutable';
import configureStore from 'redux/configureStore';
import { ConnectedRouter } from 'react-router-redux';
import AppContainer from './AppContainer';

import '../../assets/styles/main.scss';

//
// App entry point on the client side.
//

if (window.__CONFIG__) {
  try {
    window.__CONFIG__ = JSON.parse(unescape(window.__CONFIG__));
  } catch (e) {
    // TODO: not good, report error somewhere
  }
}

if (window.__ENVIRONMENT__) {
  try {
    window.__ENVIRONMENT__ = JSON.parse(unescape(window.__ENVIRONMENT__));
  } catch (e) {
    // TODO: not good, report error somewhere
  }
}

let initialState = {};
if (window.__INITIAL_STATE__) {
  try {
    let plain = JSON.parse(unescape(window.__INITIAL_STATE__));
    for (const key in plain) {
      initialState[key] = Immutable.fromJS(plain[key]);
    }
  } catch (e) {
    // TODO: not good, report error somewhere
  }
}

const store = configureStore(history, initialState);

// Scroll to top on history change
history.listen(() => {
  if (window) {
    window.scrollTo(0, 0);
  }
});

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <AppContainer />
    </ConnectedRouter>
  </Provider>
);

export default App;
