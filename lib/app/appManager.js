import { connect } from 'react-redux';
import history from 'shared/utils/history';
import { Map, Iterable } from 'immutable';
import { treatAndGetUrlWithInfo } from 'shared/utils/uriUtils';
import config from '../server/windowConfig'; // this imported config must only be used for server execution
import environment from '../server/environment'; // this imported config must only be used for server execution

/*
 * AppManager class, named AM to avoid long lines in the code, because it is to be used in many places.
 * Indeed, this class, among other things, gives all modules / components access to generic props thanks to
 * the extendProps method. Using those generic props, this class implements further methods to easily access them.
 * To avoid too much code here, consider using utility functions (utils folder) when appropriate.
 */
export default class AM {
  /*
     * Two use cases. (1) is for modules, (2) is for components ("presentational components"):
     * (1): Extend the props during a mapStateToProps function execution. Assumes argument1 is the state.
     * (2): Extend the props by only passing the component reference. Assumes argument1 to be that reference.
     */
  static extendProps(argument1, props = {}) {
    // if argument1 is a function, then it means argument1 is the component reference and we use react-redux connect
    if (typeof argument1 === 'function') {
      return connect((state) => this.extendProps(state))(argument1);
    }
    // else, argument1 is the state, and we extend the props
    props.translations = argument1.i18nReducers.get('translations');
    props.serverRequestInfo = argument1.requestState.get('serverRequestInfo');

    return props;
  }

  /*
     * Two use cases. (1) is for modules, (2) is for components ("presentational components"):
     * (1): Extend the props during a mapStateToProps function execution. Assumes argument1 is the state.
     * (2): Extend the props by only passing the component reference. Assumes argument1 to be that reference.
     */
  static apiSettings(argument1, props = {}) {
    // if argument1 is a function, then it means argument1 is the component reference and we use react-redux connect
    if (typeof argument1 === 'function') {
      return connect((state) => this.apiSettings(state))(argument1);
    }
    // else, argument1 is the state, and we extend the props

    const requestProtocol = this.val(argument1.requestState, 'serverRequestInfo', 'requestProtocol');
    const contentLanguageCode = this.val(argument1.siteSettingsState, 'siteSettings', 'contentLanguageCode');
    const countryCode = this.val(argument1.siteSettingsState, 'siteSettings', 'countryCode');
    const edge = this.val(argument1.siteSettingsState, 'siteSettings', 'edge') || new Map();

    props.apiSettings = this.val(argument1.siteSettingsState, 'siteSettings', 'api') || new Map();
    props.apiSettings = props.apiSettings.merge({
      requestProtocol,
      contentLanguageCode,
      countryCode,
      edge,
    });

    return props;
  }

  /*
     * Given a translation key, return its translation, or empty string if no translation available.
     */
  static i18n(component, translationKey) {
    if (component.props.translations && component.props.translations.has(translationKey)) {
      return component.props.translations.get(translationKey);
    }

    if (this.isDebug()) {
      const splittedKey = translationKey.split('.');

      return splittedKey[splittedKey.length - 1];
    } else {
      return '';
    }
  }

  /*
     * This is different then the regular i18n in that way it can be used with functional stateless components
      * (notice the suffix 'f' ). The regular uses component.props.translations while this expects the translations
      * directly instead of being attached to a component.
     */
  static i18nf(translations, translationKey) {
    if (translations && translations.has(translationKey)) {
      return translations.get(translationKey);
    }

    if (this.isDebug()) {
      const splittedKey = translationKey.split('.');

      return splittedKey[splittedKey.length - 1];
    } else {
      return '';
    }
  }

  static getCountryName(countryCode) {
    switch (countryCode) {
      case 'se':
        return 'sweden';
      case 'uk':
        return 'uk';
      case 'dk':
        return 'denmark';
      case 'de':
        return 'germany';
      case 'fr':
        return 'france';
      case 'at':
        return 'austria';
      default:
        return 'none';
    }
  }

  static getCountryCodeFromFlag(flagCode) {
    switch (flagCode) {
      case 'uk':
        return 'uk';
      case 'sweden':
        return 'se';
      default:
        return '';
    }
  }

  /*
     * Useful to know if a container that has fetchData method has got fetched data on the server.
     * On client side, except for the first execution, this method must always return false.
     */
  static isContainerReady(component, containerName) {
    if (this.isClient()) {
      if (this[containerName]) {
        return false;
      }
      this[containerName] = true;
    }

    return component.props.fetchedDataContainerName &&
      component.props.fetchedDataContainerName.includes(containerName);
  }

  /*
     * Return true if the user's browser user-agent parameter is considered as a mobile device.
     */
  static isMobile(component) {
    return component.props.serverRequestInfo && component.props.serverRequestInfo.get('isMobile');
  }

  /*
     * Return true if the user's browser user-agent parameter is considered as a tablet device.
     */
  static isTablet(component) {
    return component.props.serverRequestInfo && component.props.serverRequestInfo.get('isTablet');
  }

  /*
     * Either get config value using window.__CONFIG__ object on client side, or the imported config on the server side.
     */
  static config(key) {
    return this.isClient() ? window.__CONFIG__[key] : config[key];
  }

  /*
     * Either get environment value using window.__ENVIRONMENT__ object on client side, or the imported config on the server side.
     */
  static environment(key) {
    return this.isClient() ? window.__ENVIRONMENT__[key] : environment[key];
  }

  /*
     * Return whether it is client side execution.
     */
  static isClient() {
    return typeof window === 'object';
  }

  /*
     * Return whether it is debug mode.
     */
  static isDebug() {
    return this.environment('NODE_ENV') === 'development';
  }

  /*
     * Output error message only if debug mode.
     */
  static debugError(...args) {
    if (this.isDebug()) {
      console.error(new Date().toUTCString(), ...args);
    }
  }

  /*
     * Safe way to chain gets data in a Map or List (which should be arg0)
     */
  static val(arg0, ...args) {
    if (!arg0) {
      this.debugError('Error: invalid arg0 aurgument in AM.val()');

      return undefined;
    }
    let currentValue = arg0;
    for (let arg of args) {
      if (!Iterable.isIterable(currentValue)) {
        this.debugError('intermediate object not an iterable', currentValue);
        break;
      }

      currentValue = currentValue.get(arg);

      // only show error when currentValue is undefined, as emty string or 0 or null can be considered valid
      if (typeof currentValue === 'undefined') {
        this.debugError('Error: undefined value in AM.val()', arg);
        break;
      }
    }

    return currentValue;
  }

  /*
     * Go to URL programmatically. Only on client side (no reason why it should be triggered during server execution).
     * If URL is not allowed as react router link and openInNewTab is true then a new tab will be opened.
     * Return true if goToURL is with browser history (push state), false if full page load, or null if nothing happens.
     */
  static goToURL(to, usingWindowLocation, openInNewTab) {
    if (typeof to === 'string' && this.isClient()) {
      const url = treatAndGetUrlWithInfo(to);
      if (url) {
        if (!usingWindowLocation) {
          history.push(url);

          return true;
        } else {
          if (openInNewTab) {
            window.open(url, '_blank');
          } else {
            window.location = url;
          }

          return false;
        }
      }
    }

    return null;
  }
}
