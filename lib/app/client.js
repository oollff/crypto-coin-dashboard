import React from 'react';
import { hydrate } from 'react-dom';
import { CookiesProvider } from 'react-cookie';
import { AppContainer } from 'react-hot-loader'; // AppContainer is a necessary wrapper component for HMR
import App from 'app';

const render = (Component) => {
  hydrate(
    <CookiesProvider>
      <AppContainer>
        <Component />
      </AppContainer>
    </CookiesProvider>,
    document.getElementById('root')
  );
};

render(App);

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('app', () => {
    const App = require('app').default;
    render(App);
  });
}
