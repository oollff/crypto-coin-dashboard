import httpRequest from './httpRequest';
import { getBaseRequestConfig, getHostname } from './baseRequestConfig';

export function getCoinPrice(baseSymbol, askSymbol, exchange) {
  const baseRequestConfig = getBaseRequestConfig();
  const url = `${getHostname()}/api/coinPrice/getCoinPrice/${baseSymbol}/${askSymbol}/${exchange}`;

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}

