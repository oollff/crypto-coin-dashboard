import { fromJS } from 'immutable';
import axios from 'axios';
import httpRequest from './httpRequest';
import { getBaseRequestConfig } from './baseRequestConfig';

export function saveKey(key) {

  const baseRequestConfig = getBaseRequestConfig();

  const API_BASE_URL = 'http://se.pr.com:4006';
  const url = API_BASE_URL + '/api/consul/postConsulKey';

  const requestConfig = Object.assign({}, baseRequestConfig, {
    method: 'put',
    url: url,
    data: {
      key: key,
    }
  });

  return httpRequest(requestConfig);
}

export function getKey(keyName) {
  return axios.get('http://se.pr.com:4006/api/consul/getConsulKey?keyName=' + keyName)
    .then((response) => {
      return response.data;
    }).catch((err) => {
      console.log('addFolderToConsul() err:', err);
    });
}

export function deleteKey(keyName) {
  return axios.get('http://se.pr.com:4006/api/consul/deleteKey?keyName=' + keyName)
    .then((response) => {
      return response.data;
    }).catch((err) => {
      console.log('addFolderToConsul() err:', err);
    });
}

export function isItSafeToPostToConsul(keyName, state) {
  return getKey(keyName).then((key) => {
    if (key) {
      const localSourcedKey = state.consulRoute.getIn(['consulRouteState', 'consulKeys']).find(key => {
        return key.get('Key') === keyName;
      });

      const apiSourcedKey = fromJS(key);
      const isSafe = localSourcedKey.get('Value').equals(apiSourcedKey.get('Value'));

      return isSafe;
    } else {
      return false;
    }
  });
}

export function loadConsulKeys() {
  return axios.get('http://se.pr.com:4006/api/consul/getKeys/hhh');
}
