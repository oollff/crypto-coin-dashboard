import httpRequest from './httpRequest';
import { getBaseRequestConfig, getHostname } from './baseRequestConfig';

export function getCoin(symbol) {
  const baseRequestConfig = getBaseRequestConfig();

  const API_BASE_URL = getHostname();
  const url = API_BASE_URL + '/api/contentful/getCoin/' + symbol;

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}

export function getContentfulData() {
  const baseRequestConfig = getBaseRequestConfig();

  const API_BASE_URL = getHostname();
  const url = API_BASE_URL + '/api/contentful/getContentfulData';

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}

export function getCoinField(coinSymbol, fieldId) {
  const baseRequestConfig = getBaseRequestConfig();

  const url = `${getHostname()}/api/contentful/getCoinField/${coinSymbol}/${fieldId}`;

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}

export function getAllCoinIds() {
  const baseRequestConfig = getBaseRequestConfig();

  const url = `${getHostname()}/api/contentful/getAllCoinIds`;

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}

