import httpRequest from './httpRequest';
import { getBaseRequestConfig, getHostname } from './baseRequestConfig';

export function getWallet(wallet) {
  const baseRequestConfig = getBaseRequestConfig();
  const url = `${getHostname()}/api/ethplorer/getWallet/${wallet}`;

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}

