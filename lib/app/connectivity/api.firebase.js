import * as firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyBSfukl8JmDJdKpZqUv-z-T8wM3t93yZpU',
  authDomain: 'crypto-coin-audit.firebaseapp.com',
  databaseURL: 'https://crypto-coin-audit.firebaseio.com',
  projectId: 'crypto-coin-audit',
  storageBucket: 'crypto-coin-audit.appspot.com',
  messagingSenderId: '593561369409'
};

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

export const auth = firebase.auth();

// Sign Up
export const doCreateUserWithEmailAndPassword = (email, password) => {
  return auth.createUserWithEmailAndPassword(email, password);
};

// Sign In
export const doSignInWithEmailAndPassword = (email, password) =>
  auth.signInWithEmailAndPassword(email, password);

// Sign out
export const doSignOut = () =>
  auth.signOut();

// Password Reset
export const doPasswordReset = (email) =>
  auth.sendPasswordResetEmail(email);

// Password Change
export const doPasswordUpdate = (password) =>
  auth.currentUser.updatePassword(password);
