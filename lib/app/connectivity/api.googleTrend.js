import httpRequest from './httpRequest';
import { getBaseRequestConfig, getHostname } from './baseRequestConfig';

export function getGoogleTrend(keyword, startTime, endTime) {
  const baseRequestConfig = getBaseRequestConfig();
  const word = keyword.replace(new RegExp(' ', 'g'), '_');
  const start = startTime.replace(new RegExp(' ', 'g'), '_');
  const end = endTime.replace(new RegExp(' ', 'g'), '_');

  const url = `${getHostname()}/api/googleTrends/getInterestOverTime/${word}/${start}/${end}`;

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}

