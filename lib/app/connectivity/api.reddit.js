import httpRequest from './httpRequest';
import { getBaseRequestConfig, getHostname } from './baseRequestConfig';

export function getSubRedditPosts(subreddit, tab = 'new') {
  const baseRequestConfig = getBaseRequestConfig();
  const url = `${getHostname()}/api/reddit/getSubRedditPosts/${subreddit}/${tab}`;

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}
