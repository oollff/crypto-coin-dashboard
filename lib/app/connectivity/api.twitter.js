import httpRequest from './httpRequest';
import { getBaseRequestConfig, getHostname } from './baseRequestConfig';

export function getTwitterTimeline(screenName) {
  const baseRequestConfig = getBaseRequestConfig();
  const url = `${getHostname()}/api/twitter/getTwitterTimeline/${screenName}`;

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}

