import httpRequest from './httpRequest';
import { getBaseRequestConfig, getHostname } from './baseRequestConfig';

export function getChannel(channelId) {
  const baseRequestConfig = getBaseRequestConfig();
  const url = `${getHostname()}/api/youtube/getChannel/${channelId}`;

  const requestConfig = Object.assign({}, baseRequestConfig, {
    url: url,
  });

  return httpRequest(requestConfig);
}

