import React from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import { BeatLoader } from 'react-spinners';
import AbstractWidget from './AbstractWidget';

class TradingView extends React.Component {
  constructor(props) {
    super(props);
    this.ready = this.ready.bind(this);
  }

  componentWillMount() {
    if (typeof window === 'object') {
      const $script = require('scriptjs'); // eslint-disable-line global-require
      $script('https://s3.tradingview.com/tv.js', 'tradingview-widgets');
    }
  }

  shouldComponentUpdate(nextProps) {
    const changed = name => !isEqual(this.props[name], nextProps[name]);

    return changed('dataSource') || changed('options');
  }

  ready(tw, element, done) {
    const { dataSource, onReady } = this.props;

    const coinSymbol = dataSource.symbol + dataSource.compareAgainst;

    const twWidget = new tw.widget({
      container_id: 'tv_chart_container',
      autosize: true,
      symbol: coinSymbol,
      interval: '140',
      timezone: 'Etc/UTC',
      theme: 'Light',
      style: '1',
      locale: 'en',
      toolbar_bg: '#f1f3f6',
      enable_publishing: false,
      hideideas: true,
      padding: 0,
      hide_top_toolbar: true,
      hide_side_toolbar: true,
    });
    twWidget.ready(() => {
    });
  }

  render() {
    return (
      <AbstractWidget className={this.props.className} ready={this.ready} />
    );
  }
}

TradingView.defaultProps = {
  options: {},
  onLoad: () => {}
};

TradingView.Proptypes = {
  dataSource: PropTypes.object.isRequired,
  options: PropTypes.object,
  onLoad: PropTypes.func,
  className: PropTypes.string
};

export default TradingView;
