import React, { PureComponent } from 'react';
import RouterLink from 'shared/components/links/routerLink/RouterLink';
import styles from './Header.scss';
import SearchBar from './searchBar/SearchBar';
import SessionMenu from './SessionMenu';

class Header extends PureComponent {

  render() {
    return (
      <header className={styles.header}>
        <nav>
          <RouterLink className={styles.logo} to={'/'}>
            <i className="icomoon icomoon-home" />
          </RouterLink>
          <SearchBar />
          <SessionMenu />
        </nav>
      </header>
    );
  }
}

export default Header;
