import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import SignUpForm from 'shared/components/authentication/SignUpForm';
import SignInForm from 'shared/components/authentication/SignInForm';
import SignOutButton from 'shared/components/authentication/SignOutButton';
import { connect } from 'react-redux';
import style from './SessionMenu.scss';

class SessionMenu extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showSignUp: false,
      showSignIn: false,
    };

    this.handleSignUp = this.handleSignUp.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleSignUp() {
    this.setState(prevState => ({
      showSignUp: !prevState.showSignUp,
    }));
  }

  handleLogin(event) {
    this.setState(prevState => ({
      showSignIn: !prevState.showSignIn,
    }));
  }

  render() {
    const { authUser } = this.props;

    return (
      <div className={style.sessionMenu}>
        {
          authUser
            ? (
              <div>
                <span> { authUser.email.split('@')[0] } </span>
                <SignOutButton />
              </div>
            )
            : (
              <div className={style.unAuthUserMenu}>
                <input
                  type="button"
                  className={style.loginButton}
                  value="Login"
                  onClick={this.handleLogin}
                />
                {
                  this.state.showSignIn &&
                    <SignInForm />
                }
                <div className={style.divider} />
                <input
                  type="button"
                  className={style.signUpButton}
                  value="Sign Up"
                  onClick={this.handleSignUp}
                />
                {
                  this.state.showSignUp &&
                    <SignUpForm />
                }
              </div>
            )
        }
      </div>
    );
  }
}

SessionMenu.propTypes = {
  searchParam: PropTypes.string,
};

function mapStateToProps(state) {
  return {
    authUser: state.sessionState.get('authUser'),
  };
}

export default connect(mapStateToProps)(SessionMenu);
