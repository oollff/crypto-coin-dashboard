import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import AM from 'appManager';
import style from './SearchBar.scss';

class SearchBar extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      coin: props.searchParam || '',
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
    this.changeUrl = this.changeUrl.bind(this);
  }

  handleInputChange(event) {
    this.setState({ coin: event.target.value });
  }

  handleKeyPress(event) {
    if (event.key === 'Enter') {
      event.preventDefault();
      this.changeUrl();
    }
  }

  changeUrl() {
    AM.goToURL('/coin/' + this.state.coin + '/dashboard');
  }

  render() {
    return (
      <div className={style.searchBar}>
        <input
          className={style.searchInput}
          value={this.state.coin || ''}
          onChange={this.handleInputChange}
          onKeyPress={this.handleKeyPress}
        />
        <button onClick={this.changeUrl} className={style.searchButton}>
          <i className="icomoon icomoon-search" />
        </button>
      </div>
    );
  }
}

SearchBar.propTypes = {
  searchParam: PropTypes.string,
};

export default SearchBar;
