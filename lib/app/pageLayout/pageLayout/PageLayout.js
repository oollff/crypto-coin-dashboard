import React from 'react';
import PropTypes from 'prop-types';
import './PageLayout.scss';

function PageLayout({ children, className }) {
  return (
    <div className={className ? `container text-center ${className}` : 'container text-center'}>
      <div className="page-layout__viewport">
        {children}
      </div>
    </div>
  );
}

PageLayout.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};

export default PageLayout;
