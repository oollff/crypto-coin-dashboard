import { createStore, applyMiddleware, compose } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import AM from 'appManager';
import { getFromCookies } from '../shared/utils/storageUtils';
import rootReducer from './reducers';

export default function configureStore(history, initialState) {

  let middlewares = [
    thunkMiddleware,
    routerMiddleware(history),
  ];

    // only add logger middleware when allowed AND it is not production AND only on client side (perfectly fine on
    // server side as well, although the terminal would be crowded with quite long messages which would not be nice)
  if (AM.environment('CONTAINER_TAG') !== 'prod' &&
        AM.isClient() &&
        AM.config('ALLOW_LOGGER_MIDDLEWARE') === 'YES') {
    const logger = createLogger({
      level: 'info',
      collapsed: false,
      logger: console,
      predicate: () => true
    });
    middlewares.push(logger);
  }

  const createStoreWithMiddleware = compose(

    applyMiddleware(...middlewares),

    // The line (of code) below is useful only if the dev tools for Redux are installed.
    // There is a Google Chrome extension:
    // https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en
    // More information here: https://github.com/zalmoxisus/redux-devtools-extension
    hasDevtools() ?
      window.devToolsExtension({ serialize: true }) :
      f => f

  )(createStore);

  const store = createStoreWithMiddleware(rootReducer, initialState);

  return store;
}

function hasDevtools() {
  return !!(AM.isClient() && (typeof window.devToolsExtension !== 'undefined') && getFromCookies('crypto_devtools'));
}
