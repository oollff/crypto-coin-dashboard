import { combineReducers } from 'redux';
import { combineReducers as combineImmutableReducers } from 'redux-immutable';
import sessionState from 'shared/reduxStore/sessionState';
import requestState from 'shared/reduxStore/requestState';
import siteSettingsState from 'shared/reduxStore/siteSettingsState';
import routerState from 'shared/reduxStore/routerState';
import homeRouteState from 'routes/homeRoute/homeRouteState';
import coinTableState from 'routes/homeRoute/coinTable/coinTableState';
import coinRouteState from 'routes/coinRoute/coinRouteState';
import coinRouteExpandedState from 'routes/coinRoute/coinRouteExpanded/coinRouteExpandedState';
import { routerReducer as router } from 'react-router-redux';

const rootReducer = combineReducers({
  coinRouteState,
  coinRouteExpandedState,
  homeRoute: combineImmutableReducers({
    homeRouteState,
    coinTableState,
  }),
  requestState,
  routerState,
  router,
  sessionState,
  siteSettingsState,
});

export default rootReducer;
