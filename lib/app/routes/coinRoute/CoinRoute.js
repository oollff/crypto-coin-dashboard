import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { fetchCoinRouteData, toggleGraph } from './coinRouteState';
import CoinGraph from './coinGraph/CoinGraph';
import styles from './CoinRoute.scss';
import {
  renderRedditWidgetV2,
  renderRoadMapWidget,
  renderTrendsChart,
  renderTwitterTimeLine,
  renderYoutubeWidget
} from './_sharedCoinRoute/coinRouteUtils/coinRouteUtils';
import CoinInfo from './coinInfo/CoinInfo';

class CoinRoute extends PureComponent {

  static fetchData(store, params) {
    return store.dispatch(fetchCoinRouteData(params.symbol, 'server'));
  }

  componentDidMount() {
    if (!this.props.contentfulData) {
      this.props.fetchCoinRouteData(this.props.match.params.symbol, this.props.location.key);
    }
  }

  componentDidUpdate(prevProps) {
    const hasUrlKeyChanged = prevProps.location.key !== this.props.location.key;
    if (hasUrlKeyChanged) {
      this.props.fetchCoinRouteData(this.props.match.params.symbol, this.props.location.key);
    }
  }

  render() {
    if (!this.props.contentfulData) {
      return null;
    }

    const {
      contentfulData,
      errors,
      youtubeChannel,
      twitterTimeline,
      googleTrend,
      redditPosts,
      toggleGraph,
      showGraph,
    } = this.props;

    const coinSymbol = contentfulData.get('symbol');

    return (
      <div className={styles.coinRoute}>
        <CoinInfo
          iconSrc={'https://cdn4.iconfinder.com/data/icons/cryptocoins/227/ETH-512.png'}
          name={'PAY'}
          price={'$13.5'}
          timeFrame={'24h'}
          priceChange={8}
          priceHigh={'$15'}
          priceLow={'$11'}
          volume={'26m'}
          teamChange={'100'}
          whaleChange={'358'}
          toggleGraph={toggleGraph}
        />
        {
          showGraph &&
            <div className={styles.contentWrapper}>
              { coinSymbol && <CoinGraph coinSymbol={coinSymbol} /> }
            </div>
        }
        <div className={styles.contentWrapper}>
          {
            renderTrendsChart(errors, googleTrend, contentfulData.getIn(['googleTrendsKeyword', 0]))
          }
        </div>
        <div className={styles.contentWrapper}>
          { renderYoutubeWidget(errors, youtubeChannel) }
        </div>
        <div className={styles.contentWrapper}>
          { renderTwitterTimeLine(errors, twitterTimeline) }
        </div>
        <div className={styles.contentWrapper}>
          { renderRedditWidgetV2(errors, redditPosts) }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    serverRequestInfo: state.requestState.getIn(['serverRequestInfo']),
    errors: state.coinRouteState.getIn(['errors']),
    contentfulData: state.coinRouteState.getIn(['contentfulData']),
    wallets: state.coinRouteState.getIn(['wallets']),
    youtubeChannel: state.coinRouteState.getIn(['youtubeChannel']),
    googleTrend: state.coinRouteState.getIn(['googleTrend']),
    twitterTimeline: state.coinRouteState.getIn(['twitterTimeline']),
    redditPosts: state.coinRouteState.getIn(['redditPosts']),
    showGraph: state.coinRouteState.getIn(['showGraph']),
  };
}

export default connect(mapStateToProps, { fetchCoinRouteData, toggleGraph })(CoinRoute);
