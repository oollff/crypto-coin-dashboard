import React, { PureComponent } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import styles from './RedditWidget.scss';

const Post = ({ post }) => (
  <div className={styles.redditPost}>
    <a href={post.get('url')} className={styles.title}>{post.get('title')}</a>
    <div className={styles.scoreAndComments}>
      <span className={styles.score}>
        {post.get('score')} points |
      </span>
      <a href={post.get('url')} className={styles.nrOfComments}>
        {post.get('nrOfComments')} comments
      </a>
    </div>
  </div>
);

class PostListing extends PureComponent {
  render() {
    return (
      <div className={styles.redditPostListing}>
        {
          this.props.posts.map((post) => {
            return (
              <Post
                key={post.get('id')}
                post={post}
              />
            );
          })
        }
      </div>
    );
  }
}

// ------------------ Module ------------------ //

class RedditWidget extends PureComponent {
  render() {
    return (
      <div className={styles.redditWidget}>
        <h2 className={styles.header}>
          Reddit
        </h2>
        <PostListing posts={this.props.redditPosts} />
      </div>
    );
  }
}

RedditWidget.Proptypes = {
  redditPosts: ImmutablePropTypes.list.isRequired,
};

export default RedditWidget;
