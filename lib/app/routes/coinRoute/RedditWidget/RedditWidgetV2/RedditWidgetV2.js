import React, { PureComponent } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import styles from './RedditWidgetV2.scss';

const PostHeader = ({ post, children }) => (
  <div className={styles.postHeader}>
    <div className={styles.ups}>
      <span>⬆</span>
      <span>{ post.get('score') }</span>
    </div>
    <h3 className={styles.author}>
      {post.get('author')}
    </h3>
    <div className={styles.elapsedTime}>
      {post.get('elapsedTime')}
    </div>
    {
      children
    }
  </div>
);

const RedditPost = ({ post, navigatePosts, navigateCount }) => (
  <div className={styles.redditPost}>
    <PostHeader post={post}>
      <span>
        {navigateCount}
      </span>
      <i title="reddit icon" className={'icomoon icomoon-reddit ' + styles.redditIcon} />
    </PostHeader>
    <div className={styles.mainContentWithSwitcher}>
      <div className={styles.mainContent}>
        <div className={styles.title}>
          <a
            href={'https://www.reddit.com/r/TenX/comments/'+ post.get('id')}>
            {post.get('title')}
          </a>
        </div>
        <div className={styles.body}>
          {
            post.get('selfText')
              ? post.get('selfText')
              : (post.get('url').endsWith('.png') || post.get('url').endsWith('.jpg'))
              && <img className={styles.image} src={post.get('url')} alt="post image" height={100} />
          }
        </div>
      </div>
      <div className={styles.postSwitcher}>
        <i title="reddit icon" className={'icomoon icomoon-chevron-up'} onClick={() => navigatePosts(1)} />
        <div>|</div>
        <i title="reddit icon" className={'icomoon icomoon-chevron-down'}  onClick={() => navigatePosts(-1)} />
      </div>
    </div>
    <div className={styles.footer}>
      {post.get('num_comments')} comments
    </div>
    {/* <RedditAnswers />*/}
  </div>
);

const RedditComment = ({ comments }) => (
  <div className={styles.redditComment}>
    <PostHeader post={comments.get(0)} />
    <div className={styles.body}>
      {
        comments.getIn([0, 'body'])
      }
    </div>
  </div>
);

// ------------------ Module ------------------ //

class RedditWidgetV2 extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showingPostIndex: 0,
    };

    this.navigatePost = this.navigatePost.bind(this);
  }

  navigatePost(index) {
    this.setState((prevState, props) => {
      const newIndex = prevState.showingPostIndex + index;

      let indexWithinRange;
      if (newIndex >= props.redditPosts.size) {
        indexWithinRange = 0;
      } else if (newIndex < 0) {
        indexWithinRange = props.redditPosts.size -1;
      } else {
        indexWithinRange = newIndex;
      }

      return {
        showingPostIndex: indexWithinRange,
      };
    });
  }

  render() {
    const comments = this.props.redditPosts.getIn([this.state.showingPostIndex, 'comments']);

    return (
      <div className={styles.redditWidget}>
        <RedditPost
          post={this.props.redditPosts.getIn([this.state.showingPostIndex, 'post'])}
          navigateCount={`${this.state.showingPostIndex+1}/${this.props.redditPosts.size}`}
          navigatePosts={this.navigatePost}
        />
        {
          comments && comments.size > 0 &&
            <RedditComment comments={comments} />
        }
      </div>
    );
  }
}

RedditWidgetV2.Proptypes = {
  redditPosts: ImmutablePropTypes.list.isRequired,
};

export default RedditWidgetV2;
