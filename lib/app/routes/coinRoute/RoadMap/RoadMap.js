import React, { PureComponent } from 'react';
import styles from './RoadMap.scss';

export default class RoadMap extends PureComponent {

  renderListItem(milestone, index) {
    const title = milestone.getIn(['goal', 'title']);
    const metadata = milestone.getIn(['goal', 'metadata']);
    const done = milestone.get('done');
    const date = milestone.get('dueDate');

    return (
      <li key={index} className={styles.listItemWrapper}>
        <div className={styles.listItemContainer}>
          <h2><b>{date}</b> {title}</h2>
          <div>{metadata}</div>
        </div>
      </li>
    );
  }

  render() {
    const { datapoints } = this.props;

    return (
      <div className={styles.root}>
        <h2> ROAD MAP </h2>
        <ul>
          { datapoints.map((milestone, i) => this.renderListItem(milestone, i)) }
        </ul>
      </div>

    );
  }
}
