import React, { PureComponent } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { getElapsedTime } from 'shared/utils/timeDateUtils';
import { fakeEllipisise } from 'shared/utils/stringUtils';
import styles from './YoutubeVideoItem.scss';

class YoutubeVideoItem extends PureComponent {

  render() {
    const { youtubeVideoItem, navigateCount } = this.props;

    const createDate = new Date(youtubeVideoItem.getIn(['publishedAt']));

    return (
      <a
        className={styles.youtubeVideoItem}
        href={`https://www.youtube.com/watch?v=${youtubeVideoItem.getIn(['videoId'])}`}>
        <div>
          { renderThumbNail(youtubeVideoItem.getIn(['thumbnails'])) }
        </div>
        <div className={styles.textContainer}>
          <div className={styles.header}>
            <h3 className={styles.title}>
              {fakeEllipisise(youtubeVideoItem.getIn(['title']), 55)}
            </h3>
            <i className={'icomoon icomoon-youtube ' + styles.youtubeIcon} />
          </div>
          <div className={styles.body}>
            {
              youtubeVideoItem.getIn(['title']).length < 49 &&
                <div className={styles.description}>
                  {fakeEllipisise(youtubeVideoItem.getIn(['description']), 60)}
                </div>
            }
            <span className={styles.date}>
              {getElapsedTime(createDate)}
            </span>
          </div>
        </div>
      </a>
    );
  }
}

function renderThumbNail(thumbnails) {
  return (
    <img
      src={thumbnails.getIn(['medium', 'url'])}
      width={thumbnails.getIn(['medium', 'width']) / 2.2}
      height={thumbnails.getIn(['medium', 'height']) / 2.2}
      alt="youtube video item"
    />
  );
}

YoutubeVideoItem.propTypes = {
  youtubeVideoItem: ImmutablePropTypes.map.isRequired,
};

export default YoutubeVideoItem;
