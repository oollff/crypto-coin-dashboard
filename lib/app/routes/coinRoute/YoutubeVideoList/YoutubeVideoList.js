import React, { PureComponent } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import YoutubeVideoItem from './YoutubeVideoItem';
import styles from './YoutubeVideoList.scss';

class YoutubeVideoList extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showingYoutubeIndex: 0,
    };

    this.navigateYoutubeItems = this.navigateYoutubeItems.bind(this);
  }

  navigateYoutubeItems(index) {
    this.setState((prevState, props) => {
      const newIndex = prevState.showingYoutubeIndex + index;

      let indexWithinRange;
      if (newIndex >= props.youtubeVideos.size) {
        indexWithinRange = 0;
      } else if (newIndex < 0) {
        indexWithinRange = props.youtubeVideos.size -1;
      } else {
        indexWithinRange = newIndex;
      }

      return {
        showingYoutubeIndex: indexWithinRange,
      };
    });
  }

  render() {
    const { youtubeVideos } = this.props;

    return (
      <div key="YoutubeVideoList" className={styles.youtubeVideoList}>
        <YoutubeVideoItem
          navigateCount={`${this.state.showingYoutubeIndex+1}/${this.props.youtubeVideos.size}`}
          youtubeVideoItem={youtubeVideos.getIn([this.state.showingYoutubeIndex])}
        />
        <div className={styles.postSwitcher}>
          <i
            title="up icon"
            className={'icomoon icomoon-chevron-up ' + styles.navigationContent}
            onClick={() => this.navigateYoutubeItems(1)}
          />
          <span className={styles.counter}>
            {`${this.state.showingYoutubeIndex+1}/${this.props.youtubeVideos.size}`}
          </span>
          <i
            title="down icon"
            className={'icomoon icomoon-chevron-down ' + styles.navigationContent}
            onClick={() => this.navigateYoutubeItems(-1)}
          />
        </div>
      </div>
    );
  }
}

YoutubeVideoList.propTypes = {
  youtubeVideos: ImmutablePropTypes.list.isRequired,
};

export default YoutubeVideoList;
