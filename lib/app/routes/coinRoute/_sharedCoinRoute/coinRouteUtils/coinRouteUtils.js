import React from 'react';
import { ERRORS } from 'routes/coinRoute/coinRouteState';
import TwitterTimeLine from 'routes/coinRoute/twitterTimeLine/TwitterTimeLine';
import Youtube from 'routes/coinRoute/YoutubeVideoList';
import RoadMap from 'routes/coinRoute/RoadMap';
import TrendsChart from 'routes/coinRoute/trendsChart/TrendsChart';
import RedditWidget from 'routes/coinRoute/RedditWidget';
import RedditWidgetV2 from 'routes/coinRoute/RedditWidget/RedditWidgetV2';

export function renderRoadMapWidget(contentfulData) {
  const coinRoadMap = contentfulData.get('roadMap');

  return coinRoadMap && (<RoadMap datapoints={coinRoadMap} />);
}

export function renderRedditWidget(errors, redditPosts) {
  const error = errors.get(ERRORS.FETCH_AND_HANDLE_TWITTER_TIMELINE);
  if (error) {
    return <div>{ error }</div>;
  }

  return redditPosts && ( <RedditWidget redditPosts={redditPosts} /> );
}

export function renderRedditWidgetV2(errors, redditPosts) {
  const error = errors.get(ERRORS.FETCH_AND_HANDLE_REDDIT);
  if (error) {
    return <div>{ error }</div>;
  }

  return redditPosts && ( <RedditWidgetV2 redditPosts={redditPosts} /> );
}

export function renderTwitterTimeLine(errors, twitterTimeline) {
  const error = errors.get(ERRORS.FETCH_AND_HANDLE_TWITTER_TIMELINE);
  if (error) {
    return <div>{ error }</div>;
  }

  return twitterTimeline && ( <TwitterTimeLine twitterTimeline={twitterTimeline} /> );
}

export function renderTrendsChart(errors, googleTrend, keyword) {
  const error = errors.get(ERRORS.FETCH_AND_HANDLE_TWITTER_TIMELINE);
  if (error) {
    return <div>{ error }</div>;
  }

  return googleTrend && ( <TrendsChart googleTrend={googleTrend.toJS()} keyWord={keyword} /> );
}

export function renderYoutubeWidget(errors, youtubeChannel) {
  const error = errors.get(ERRORS.FETCH_AND_HANDLE_YOUTUBE_CHANNEL);
  if (error) {
    return <div>{ error }</div>;
  }

  return youtubeChannel && ( <Youtube youtubeVideos={youtubeChannel} /> );
}
