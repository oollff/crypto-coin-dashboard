import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import TradingView from 'libs/reactTradingViewWidgets/TradingView';
import styles from './CoinGraph.scss';

class CoinGraph extends PureComponent {
  render() {
    const { coinSymbol } = this.props;

    return (
      <TradingView
        className={styles.tradingViewWidget}
        dataSource={{
          symbol: coinSymbol,
          compareAgainst: 'USD',
        }}
      />
    );
  }
}

CoinGraph.propTypes = {
  coinSymbol: PropTypes.string.isRequired,
};

export default CoinGraph;
