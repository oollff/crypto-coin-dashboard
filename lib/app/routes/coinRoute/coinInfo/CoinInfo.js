import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames/bind';
import styles from './CoinInfo.scss';

function renderCoinIcon(url, alt, className) {
  return (
    <img
      className={className}
      src={url}
      width={20}
      height={20}
      alt={alt}
    />
  );
}

class CoinInfo extends PureComponent {
  getPriceChangeStyle(priceChange) {
    return classNames({
      [styles.negativePriceChange]: !(priceChange >= 0),
      [styles.positivePriceChange]: priceChange >= 0,
    });
  }

  render() {
    const {
      toggleGraph,
      iconSrc,
      name,
      price,
      timeFrame,
      priceChange,
      priceHigh,
      priceLow,
      volume,
      teamChange,
      whaleChange
    } = this.props;

    return (
      <div className={styles.coinInfo}>
        <div className={styles.coinInfoRow}>
          <div className={`${styles.coinInfoGroup} ${styles.priceName}`}>
            <div className={styles.groupItem}>
              { renderCoinIcon(iconSrc, name + 'icon') }
            </div>
            <h1 className={styles.groupItem}>
              { name }
            </h1>
            <div className={`${styles.groupItem} ${styles.price}`}>
              { price }
            </div>
          </div>
          <div className={`${styles.coinInfoGroup} ${styles.timeFrame}`}>
            <div className={styles.groupItem}>
              { timeFrame }
            </div>
            <div className={`${styles.groupItem} ${styles.priceChange} ${this.getPriceChangeStyle(priceChange)}`}>
              { priceChange }%
            </div>
          </div>
          <div className={`${styles.coinInfoGroup} ${styles.teamWhale}`}>
            <div className={`${styles.groupItem} ${styles.teamGroup}`}>
              <i title="team icon" className={'icomoon icomoon-team'} />
              <div className={`${styles.teamChange}`}>
                { teamChange }
              </div>
            </div>
            <div className={`${styles.groupItem} ${styles.whaleGroup}`}>
              <i title="whales" className={'icomoon icomoon-grooveshark'} />
              <div className={`${styles.whaleChange}`}>
                { whaleChange }
              </div>
            </div>
          </div>
        </div>
        <div className={styles.coinInfoRow}>
          <div className={styles.secondaryInfo}>
            <div className={styles.priceHigh}>
              High: {priceHigh}
            </div>
            <div className={styles.priceLow}>
              Low: {priceLow}
            </div>
            <div className={styles.volume}>
              Volume: {volume}
            </div>
            <div onClick={toggleGraph} className={styles.showGraphButton}>
              <i className={'icomoon icomoon-stats-dots'} />
              <i className={'icomoon icomoon-keyboard_arrow_down'} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CoinInfo.propTypes = {
  iconSrc: PropTypes.string,
  name: PropTypes.string,
  timeFrame: PropTypes.string,
  price: PropTypes.string,
  priceChange: PropTypes.number,
  priceHigh: PropTypes.string,
  priceLow: PropTypes.string,
  volume: PropTypes.string,
  teamChange: PropTypes.string,
  whaleChange: PropTypes.string,
  toggleGraph: PropTypes.func,
};

export default CoinInfo;
