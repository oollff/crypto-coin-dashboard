import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { fromJS } from 'immutable';
import AM from 'appManager';
import Carousel from 'shared/components/carousel/Carousel';
import TwitterTimeLine from '../twitterTimeLine/TwitterTimeLine';
import TeamHoldings from '../teamHoldings/TeamHoldings';
import Youtube from '../YoutubeVideoList';
import CoinGraph from '../coinGraph/CoinGraph';
import RoadMap from '../RoadMap';
import { ERRORS, fetchCoinRouteData } from './coinRouteExpandedState';
import styles from './CoinRouteExpanded.scss';

class CoinRouteExpanded extends PureComponent {

  static fetchData(store, params) {
    return store.dispatch(fetchCoinRouteData(params.symbol, 'server'));
  }

  componentDidMount() {
    if (!this.props.contentfulData) {
      this.props.fetchCoinRouteData(this.props.match.params.symbol, this.props.location.key);
    }
  }

  componentDidUpdate(prevProps) {
    const hasUrlKeyChanged = prevProps.location.key !== this.props.location.key;
    if (hasUrlKeyChanged) {
      this.props.fetchCoinRouteData(this.props.match.params.symbol, this.props.location.key);
    }
  }

  render() {
    if (!this.props.contentfulData) {
      return null;
    }

    const { contentfulData, errors, wallets, youtubeChannel, twitterTimeline } = this.props;

    const coinSymbol = contentfulData.get('symbol');

    return (
      AM.isMobile(this) || AM.isTablet(this)?
        renderMobileView(coinSymbol, errors, wallets, contentfulData, youtubeChannel, twitterTimeline)
        : (
          <div className={styles.coinRouteExpanded}>
            <div className={`${styles.priceHoldingsColumn}`}>
              <div className={styles.fiftyRow}>
                { coinSymbol && <CoinGraph coinSymbol={coinSymbol} /> }
              </div>
              <div className={styles.fiftyRow}>
                {
                  renderTeamHoldingsWidget(coinSymbol, errors, wallets)
                }
              </div>
            </div>
            <div className={`${styles.widgetColumn}`}>
              <div className={styles.fiftyRow}>
                {
                  renderTwitterTimeLine(errors, twitterTimeline)
                }
              </div>
            </div>
            <div className={`${styles.widgetColumn}`}>
              <div className={styles.fiftyRow}>
                {
                  renderRoadMapWidget(contentfulData)
                }
              </div>
            </div>
            <div className={`${styles.widgetColumn}`}>
              <div className={styles.fiftyRow}>
                {
                  renderYoutubeWidget(errors, youtubeChannel)
                }
              </div>
            </div>
          </div>
        )
    );
  }
}

function renderMobileView(coinSymbol, errors, wallets, contentfulData, youtubeChannel, twitterTimeline) {
  const SETTINGS = fromJS({
    nbrOfSlidesInView: 1,
    showArrows: true,
    showDots: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          nbrOfSlidesInView: 3,
        },
      },
      {
        breakpoint: 1024,
        settings: {
          nbrOfSlidesInView: 4,
        },
      },
      {
        breakpoint: 1279,
        settings: {
          nbrOfSlidesInView: 5,
        },
      },
    ],
  });

  const items = [
    <CoinGraph coinSymbol={coinSymbol} />,
    renderTeamHoldingsWidget(coinSymbol, errors, wallets),
    renderTwitterTimeLine(errors, twitterTimeline),
    renderRoadMapWidget(contentfulData),
    renderYoutubeWidget(errors, youtubeChannel)
  ];

  return (
    <div className={styles.carousel}>
      {
        <Carousel
          lazyLoad={true}
          settings={SETTINGS} >
          {
            items.map((Item, index) => {
              return (
                <div className={styles.widgetColumn}>
                  { Item }
                </div>
              );
            })
          }
        </Carousel>
      }
    </div>
  );
}

function renderRoadMapWidget(contentfulData) {
  const coinRoadMap = contentfulData.get('roadMap');

  return coinRoadMap && <RoadMap datapoints={coinRoadMap} />;
}

function renderTeamHoldingsWidget(coinSymbol, errors, wallets) {
  const teamHoldingFetchingError = errors.get(ERRORS.FETCH_AND_HANDLE_WALLETS);
  if (teamHoldingFetchingError) {
    return <div>{ teamHoldingFetchingError }</div>;
  }

  return wallets && ( <TeamHoldings wallets={wallets} coinSymbol={coinSymbol} /> );
}

function renderTwitterTimeLine(errors, twitterTimeline) {
  const error = errors.get(ERRORS.FETCH_AND_HANDLE_TWITTER_TIMELINE);
  if (error) {
    return <div>{ error }</div>;
  }

  return twitterTimeline && ( <TwitterTimeLine twitterTimeline={twitterTimeline} /> );
}

function renderYoutubeWidget(errors, youtubeChannel) {
  const error = errors.get(ERRORS.FETCH_AND_HANDLE_YOUTUBE_CHANNEL);
  if (error) {
    return <div>{ error }</div>;
  }

  return youtubeChannel && ( <Youtube youtubeVideos={youtubeChannel} /> );
}

function mapStateToProps(state) {
  return {
    errors: state.coinRouteExpandedState.getIn(['errors']),
    contentfulData: state.coinRouteExpandedState.getIn(['contentfulData']),
    wallets: state.coinRouteExpandedState.getIn(['wallets']),
    youtubeChannel: state.coinRouteExpandedState.getIn(['youtubeChannel']),
    twitterTimeline: state.coinRouteExpandedState.getIn(['twitterTimeline']),
    serverRequestInfo: state.requestState.getIn(['serverRequestInfo']),
  };
}

export default connect(mapStateToProps, { fetchCoinRouteData })(CoinRouteExpanded);
