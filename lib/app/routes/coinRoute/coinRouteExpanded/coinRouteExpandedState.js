import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { getCoin } from 'connectivity/api.contentful';
import { getWallet } from 'connectivity/api.ethplorer';
import { getChannel } from 'connectivity/api.youtube';
import { getTwitterTimeline } from 'connectivity/api.twitter';
import { getFreshState } from 'shared/utils/routerUtils/routerUtils';

export const COIN_ROUTE_EXPANDED_ERROR = Symbol('COIN_ROUTE_EXPANDED_ERROR');
export const FETCH_COIN_EXPANDED_SUCCESS = Symbol('FETCH_COIN_EXPANDED_SUCCESS');
export const FETCH_AND_HANDLE_EXPANDED_WALLETS_SUCCESS = Symbol('FETCH_AND_HANDLE_EXPANDED_WALLETS_SUCCESS');
export const FETCH_AND_HANDLE_EXPANDED_YOUTUBE_CHANNEL_SUCCESS
  = Symbol('FETCH_AND_HANDLE_EXPANDED_YOUTUBE_CHANNEL_SUCCESS');
export const FETCH_AND_HANDLE_TWITTER_TIMELINE_EXPANDED_SUCCESS
  = Symbol('FETCH_AND_HANDLE_TWITTER_TIMELINE_EXPANDED_SUCCESS');

export const ERRORS = {
  FETCH_COIN: '',
  FETCH_AND_HANDLE_WALLETS: '',
  FETCH_AND_HANDLE_YOUTUBE_CHANNEL: '',
  FETCH_AND_HANDLE_TWITTER_TIMELINE: '',
};

let defaultState = fromJS({
  contentfulData: null,
  urlKey: '',
  wallets: null,
  youtubeChannel: null,
  twitterTimeline: null,
  errors: ERRORS,
});

export default function(state = defaultState, action) {
  switch (action.type) {
    case LOCATION_CHANGE: {
      const regex = new RegExp('^/coin/.*/dashboard/expanded(?!/)');

      return getFreshState(state, defaultState, regex, action.payload, state.get('urlKey'));
    }
    case FETCH_COIN_EXPANDED_SUCCESS:
      return state.merge({
        urlKey: action.data.urlKey,
        contentfulData: action.data.contentfulData,
        errorMessage: '',
      });
    case FETCH_AND_HANDLE_EXPANDED_WALLETS_SUCCESS:
      return state.merge({ 'wallets': action.data }).setIn(['errors', 'FETCH_AND_HANDLE_WALLETS'], '');
    case FETCH_AND_HANDLE_EXPANDED_YOUTUBE_CHANNEL_SUCCESS:
      return state.merge({ 'youtubeChannel': action.data }).setIn(['errors', 'FETCH_AND_HANDLE_YOUTUBE_CHANNEL'], '');
    case FETCH_AND_HANDLE_TWITTER_TIMELINE_EXPANDED_SUCCESS:
      return state.merge({ 'twitterTimeline': action.data }).setIn(['errors', 'FETCH_AND_HANDLE_TWITTER_TIMELINE'], '');
    case COIN_ROUTE_EXPANDED_ERROR:
      return state.setIn(['errors', action.type, action.message]);
    default:
      return state;
  }
}

export function fetchCoinRouteData(coinName, urlKey) {
  return function (dispatch, getState) {
    return dispatch(fetchAndHandleCoin(coinName, urlKey)).then(() => {
      const promises = [];

      const wallets = getState().coinRouteExpandedState.getIn(['contentfulData', 'wallets']);
      if (wallets) { promises.push(dispatch(fetchAndHandleWallets(wallets))); }

      const channelId = getState().coinRouteExpandedState.getIn(['contentfulData', 'youtubeChannelId']);
      if (channelId) { promises.push(dispatch(fetchAndHandleYoutubeChannel(channelId))); }

      const twitterScreenName = getState().coinRouteExpandedState.getIn(['contentfulData', 'twitterScreenName']);
      if (twitterScreenName) { promises.push(dispatch(fetchAndHandleTwitterTimeline(twitterScreenName))); }

      return Promise.all(promises);
    });
  };
}

function coinRouteError(error) {
  return {
    type: COIN_ROUTE_EXPANDED_ERROR,
    error: error,
  };
}

function fetchAndHandleCoin(coinName, urlKey) {
  return function (dispatch) {
    return getCoin(coinName).then((response) => {
      dispatch({
        type: FETCH_COIN_EXPANDED_SUCCESS,
        data: { contentfulData: response.data, urlKey: urlKey, },
      });
    }).catch(() => {
      dispatch(coinRouteError({ type: ERRORS.FETCH_COIN, message: 'Fetching consul key error', }));
    });
  };
}

function fetchAndHandleWallets(wallets) {
  return function (dispatch) {
    // Create an array of promises inside of promise all to be executed async and collect the data in
    // a single callback when all promises are done. Needs to be wrapped inside Promise.all.
    return Promise.all(wallets.map( wallet => getWallet(wallet.get('walletId')))).then((responseArray) => {
      const wallets = responseArray.map(wallet => {
        return wallet.data;
      });

      return dispatch({
        type: FETCH_AND_HANDLE_EXPANDED_WALLETS_SUCCESS,
        data: wallets,
      });
    }).catch(() => {
      dispatch(coinRouteError({ type: ERRORS.FETCH_AND_HANDLE_WALLETS, message: 'Fetching wallet error', }));
    });
  };
}

function fetchAndHandleYoutubeChannel(channelId) {
  return function (dispatch) {
    // Create an array of promises inside of promise all to be executed async and collect the data in
    // a single callback when all promises are done. Needs to be wrapped inside Promise.all.
    return getChannel(channelId).then((response) => {
      return dispatch({
        type: FETCH_AND_HANDLE_EXPANDED_YOUTUBE_CHANNEL_SUCCESS,
        data: response.data,
      });
    }).catch(() => {
      dispatch(coinRouteError({
        type: ERRORS.FETCH_AND_HANDLE_YOUTUBE_CHANNEL,
        message: 'Fetching youtube channel error',
      }));
    });
  };
}

function fetchAndHandleTwitterTimeline(twitterScreenName) {
  return function (dispatch) {
    return getTwitterTimeline(twitterScreenName).then((response) => {
      return dispatch({
        type: FETCH_AND_HANDLE_TWITTER_TIMELINE_EXPANDED_SUCCESS,
        data: response.data,
      });
    }).catch(() => {
      dispatch(coinRouteError({
        type: ERRORS.FETCH_AND_HANDLE_TWITTER_TIMELINE,
        message: 'Fetching twitter error',
      }));
    });
  };
}

