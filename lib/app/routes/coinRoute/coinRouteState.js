import { fromJS } from 'immutable';
import { getCoin } from 'connectivity/api.contentful';
import { LOCATION_CHANGE } from 'react-router-redux';
import { getWallet } from 'connectivity/api.ethplorer';
import { getChannel } from 'connectivity/api.youtube';
import { getTwitterTimeline } from 'connectivity/api.twitter';
import { getGoogleTrend } from 'connectivity/api.googleTrend';
import { getSubRedditPosts } from 'connectivity/api.reddit';
import { getFreshState } from 'shared/utils/routerUtils/routerUtils';

export const TOGGLE_GRAPH = Symbol('TOGGLE_GRAPH');
export const COIN_ROUTE_ERROR = Symbol('COIN_ROUTE_ERROR');
export const FETCH_COIN_SUCCESS = Symbol('FETCH_COIN_SUCCESS');
export const FETCH_AND_HANDLE_WALLETS_SUCCESS = Symbol('FETCH_AND_HANDLE_WALLETS_SUCCESS');
export const FETCH_AND_HANDLE_YOUTUBE_CHANNEL_SUCCESS = Symbol('FETCH_AND_HANDLE_YOUTUBE_CHANNEL_SUCCESS');
export const FETCH_AND_HANDLE_TWITTER_TIMELINE_SUCCESS = Symbol('FETCH_AND_HANDLE_TWITTER_TIMELINE_SUCCESS');
export const FETCH_AND_HANDLE_GOOGLE_TREND_SUCCESS = Symbol('FETCH_AND_HANDLE_GOOGLE_TREND_SUCCESS');
export const FETCH_AND_HANDLE_REDDIT_SUCCESS = Symbol('FETCH_AND_HANDLE_REDDIT_SUCCESS');

export const ERRORS = {
  FETCH_COIN: '',
  FETCH_AND_HANDLE_WALLETS: '',
  FETCH_AND_HANDLE_YOUTUBE_CHANNEL: '',
  FETCH_AND_HANDLE_TWITTER_TIMELINE: '',
  FETCH_AND_HANDLE_GOOGLE_TREND: '',
  FETCH_AND_HANDLE_REDDIT: '',
};

let defaultState = fromJS({
  contentfulData: null,
  urlKey: '',
  wallets: null,
  youtubeChannel: null,
  twitterTimeline: null,
  googleTrend: null,
  redditPosts: null,
  showGraph: false,
  errors: ERRORS,
});

export default function(state = defaultState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      const regex = new RegExp('^/coin/.*/dashboard(?!/)');

      return getFreshState(state, defaultState, regex, action.payload, state.get('urlKey'));
    case FETCH_COIN_SUCCESS:
      return state.merge({
        urlKey: action.data.urlKey,
        contentfulData: action.data.contentfulData,
        errorMessage: '',
      });
    case FETCH_AND_HANDLE_WALLETS_SUCCESS:
      return state.merge({ 'wallets': action.data }).setIn(['errors', 'FETCH_AND_HANDLE_WALLETS'], '');
    case FETCH_AND_HANDLE_YOUTUBE_CHANNEL_SUCCESS:
      return state.merge({ 'youtubeChannel': action.data }).setIn(['errors', 'FETCH_AND_HANDLE_YOUTUBE_CHANNEL'], '');
    case FETCH_AND_HANDLE_TWITTER_TIMELINE_SUCCESS:
      return state.merge({ 'twitterTimeline': action.data }).setIn(['errors', 'FETCH_AND_HANDLE_TWITTER_TIMELINE'], '');
    case FETCH_AND_HANDLE_GOOGLE_TREND_SUCCESS:
      return state.merge({ 'googleTrend': action.data }).setIn(['errors', 'FETCH_AND_HANDLE_GOOGLE_TREND'], '');
    case FETCH_AND_HANDLE_REDDIT_SUCCESS:
      return state.merge({ 'redditPosts': action.data }).setIn(['errors', 'FETCH_AND_HANDLE_REDDIT'], '');
    case TOGGLE_GRAPH:
      return state.set('showGraph', !state.get('showGraph'));
    case COIN_ROUTE_ERROR:
      return state.setIn(['errors', action.type, action.message]);
    default:
      return state;
  }
}

function coinRouteError(error) {
  return {
    type: COIN_ROUTE_ERROR,
    error: error,
  };
}

function fetchAndHandleCoin(coinName, urlKey) {
  return function (dispatch) {
    return getCoin(coinName).then((response) => {
      dispatch({
        type: FETCH_COIN_SUCCESS,
        data: { contentfulData: response.data, urlKey: urlKey, },
      });
    }).catch(() => {
      dispatch(coinRouteError({ type: ERRORS.FETCH_COIN, message: 'Fetching consul key error', }));
    });
  };
}

function fetchAndHandleWallets(wallets) {
  return function (dispatch) {
    // Create an array of promises inside of promise all to be executed async and collect the data in
    // a single callback when all promises are done. Needs to be wrapped inside Promise.all.
    return Promise.all(wallets.map( wallet => getWallet(wallet.get('walletId')))).then((responseArray) => {
      const wallets = responseArray.map(wallet => {
        return wallet.data;
      });

      return dispatch({
        type: FETCH_AND_HANDLE_WALLETS_SUCCESS,
        data: wallets,
      });
    }).catch(() => {
      dispatch(coinRouteError({ type: ERRORS.FETCH_AND_HANDLE_WALLETS, message: 'Fetching wallet error', }));
    });
  };
}

function fetchAndHandleYoutubeChannel(channelId) {
  return function (dispatch) {
    return getChannel(channelId).then((response) => {
      return dispatch({
        type: FETCH_AND_HANDLE_YOUTUBE_CHANNEL_SUCCESS,
        data: response.data,
      });
    }).catch(() => {
      dispatch(coinRouteError({
        type: ERRORS.FETCH_AND_HANDLE_YOUTUBE_CHANNEL,
        message: 'Fetching youtube channel error',
      }));
    });
  };
}

function fetchAndHandleTwitterTimeline(twitterScreenName) {
  return function (dispatch) {
    return getTwitterTimeline(twitterScreenName).then((response) => {
      return dispatch({
        type: FETCH_AND_HANDLE_TWITTER_TIMELINE_SUCCESS,
        data: response.data,
      });
    }).catch(() => {
      dispatch(coinRouteError({
        type: ERRORS.FETCH_AND_HANDLE_TWITTER_TIMELINE,
        message: 'Fetching twitter error',
      }));
    });
  };
}

function fetchAndHandleGoogleTrendData(keyword, startTime, endTime) {
  return function (dispatch) {
    return getGoogleTrend(keyword, startTime, endTime).then((response) => {
      return dispatch({
        type: FETCH_AND_HANDLE_GOOGLE_TREND_SUCCESS,
        data: response.data,
      });
    }).catch(() => {
      dispatch(coinRouteError({
        type: ERRORS.FETCH_AND_HANDLE_GOOGLE_TREND,
        message: 'Fetching google trend error',
      }));
    });
  };
}

function fetchAndHandleReddit(subReddit, tab) {
  return function (dispatch) {
    return getSubRedditPosts(subReddit, tab).then((response) => {
      return dispatch({
        type: FETCH_AND_HANDLE_REDDIT_SUCCESS,
        data: response.data,
      });
    }).catch(() => {
      dispatch(coinRouteError({
        type: ERRORS.FETCH_AND_HANDLE_REDDIT,
        message: 'Fetching reddit error',
      }));
    });
  };
}

export function toggleGraph() {
  return {
    type: TOGGLE_GRAPH,
  };
}

export function fetchCoinRouteData(coinName, urlKey) {
  return function (dispatch, getState) {
    return dispatch(fetchAndHandleCoin(coinName, urlKey)).then(() => {
      const promises = [];

      const wallets = getState().coinRouteState.getIn(['contentfulData', 'wallets']);
      if (wallets) { promises.push(dispatch(fetchAndHandleWallets(wallets))); }

      const channelId = getState().coinRouteState.getIn(['contentfulData', 'youtubeChannelId']);
      if (channelId) { promises.push(dispatch(fetchAndHandleYoutubeChannel(channelId))); }

      const twitterScreenName = getState().coinRouteState.getIn(['contentfulData', 'twitterScreenName']);
      if (twitterScreenName) { promises.push(dispatch(fetchAndHandleTwitterTimeline(twitterScreenName))); }

      const googleTrendKeyword = getState().coinRouteState.getIn(['contentfulData', 'googleTrendsKeyword', 0]);
      if (googleTrendKeyword) {
        let pastDate = new Date();
        pastDate.setMonth(pastDate.getMonth() - 3);
        const todaysDate = new Date().toDateString();
        promises.push(dispatch(fetchAndHandleGoogleTrendData(googleTrendKeyword, pastDate.toDateString(), todaysDate)));
      }

      const subReddit = getState().coinRouteState.getIn(['contentfulData', 'subReddit']);
      if (subReddit) { promises.push(dispatch(fetchAndHandleReddit(subReddit))); }

      return Promise.all(promises);
    });
  };
}
