import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { BeatLoader } from 'react-spinners';
import Wallet from './wallet/Wallet';
import styles from './TeamHoldings.scss';

class TeamHoldings extends PureComponent {
  render() {
    const { wallets, coinSymbol } = this.props;

    return ([
      (
        wallets
          ? (
            <div key="TeamHoldings" className={styles.TeamHoldings}>
              {
                wallets && wallets.map(wallet => {
                  return (
                    <Wallet
                      key={wallet.get('address')}
                      teamSymbol={coinSymbol}
                      wallet={ wallet }
                    />
                  );
                })
              }
            </div>
          )
          : <BeatLoader key="TeamHoldings" />
      )
    ]);
  }
}

TeamHoldings.propTypes = {
  coinSymbol: PropTypes.string,
  wallets: ImmutablePropTypes.list.isRequired,
};

export default TeamHoldings;
