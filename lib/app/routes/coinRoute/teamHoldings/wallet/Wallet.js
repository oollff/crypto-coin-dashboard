import React, { PureComponent } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import PropTypes from 'prop-types';
import styles from './Wallet.scss';

class Wallet extends PureComponent {

  getTeamToken() {
    return this.props.wallet.get('tokens').find(token => {
      return token.getIn(['tokenInfo', 'symbol']) === this.props.teamSymbol;
    });
  }

  render() {
    const { wallet } = this.props;
    const teamToken = this.getTeamToken();

    return (
      <div className={styles.wallet}>
        <p>address: { wallet.getIn(['address']) }</p>
        <p>countTxs: { wallet.getIn(['countTxs']) }</p>
        <p>ETH balance: { wallet.getIn(['ETH', 'balance']) }</p>
        {
          teamToken &&
            <p>{ `${this.props.teamSymbol}: ${teamToken.getIn(['balance'])}` }</p>
        }
      </div>
    );
  }
}

Wallet.propTypes = {
  teamSymbol: PropTypes.string,
  wallet: ImmutablePropTypes.map,
};

export default Wallet;
