import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import {
  ResponsiveContainer,
  Line,
  XAxis,
  YAxis,
  Tooltip,
  CartesianGrid,
  LineChart,
  Legend,
} from 'recharts';
import classNames from 'classnames/bind';
import styles from './TrendsChart.scss';

class TrendsChart extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showGoogleTrends: false,
    };
  }

  getLastestDay(googleTrend) {
    const yesterDay = googleTrend[googleTrend.length - 2].formattedValue;
    const today = googleTrend[googleTrend.length -1].formattedValue;
    const change = ((today - yesterDay) / yesterDay * 100).toFixed(2);

    const className = classNames({
      [styles.negativePriceChange]: !(change >= 0),
      [styles.positivePriceChange]: change >= 0,
    });

    return (
      <span className={className}>
        {`${change}%`}
      </span>
    );
  }

  render() {
    const { googleTrend, keyWord } = this.props;

    return (
      <div className={styles.trendsChart}>
        <div className={styles.trendsHeader}>
          <h2>Google trends</h2>
          {
            this.getLastestDay(googleTrend)
          }
          <div onClick={() => this.setState((prev) => ({ showGoogleTrends: !prev.showGoogleTrends }))}>Show graph</div>
        </div>
        {
          this.state.showGoogleTrends &&
            <div className={styles.chart}>
              <ResponsiveContainer
                width="100%" >
                <LineChart
                  data={googleTrend}
                  margin={{ right: 30, left: -40, top: 2, bottom: 2 }}
                  syncId="primaryChart">
                  <XAxis
                    dataKey="formattedTime"
                    axisLine={false}
                    tickLine={false}
                  />
                  <YAxis
                    domain={['auto', 'auto']}
                    axisLine={false}
                    tickLine={false}
                  />
                  <Tooltip
                    isAnimationActive={false}
                  />
                  <Legend verticalAlign="top" height={36} />
                  <CartesianGrid
                    vertical={false}
                    stroke="#e4e4e4"
                  />
                  <Line
                    name={ keyWord }
                    isAnimationActive={false}
                    type="linear"
                    dataKey="formattedValue"
                    stroke="#337ab7"
                    strokeWidth={2}
                    dot={false}
                    activeDot={{ fill: '#2f5aad', stroke: 'none', r: 3 }}
                  />
                </LineChart>
              </ResponsiveContainer>
            </div>
        }
      </div>
    );
  }
}

TrendsChart.propTypes = {
  googleTrend: PropTypes.array,
  keyWord: PropTypes.string,
};

export default TrendsChart;
