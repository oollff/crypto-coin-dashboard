import React, { PureComponent } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import TweetItem from './tweetItem/TweetItem';
import styles from './TwitterTimeLine.scss';

class TwitterTimeLine extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showingTweetIndex: 0,
    };

    this.navigateTweets = this.navigateTweets.bind(this);
  }

  navigateTweets(index) {
    this.setState((prevState, props) => {
      const newIndex = prevState.showingTweetIndex + index;

      let indexWithinRange;
      if (newIndex >= props.twitterTimeline.size) {
        indexWithinRange = 0;
      } else if (newIndex < 0) {
        indexWithinRange = props.twitterTimeline.size -1;
      } else {
        indexWithinRange = newIndex;
      }

      return {
        showingTweetIndex: indexWithinRange,
      };
    });
  }

  render() {
    const { twitterTimeline } = this.props;

    return (
      <div key="YoutubeVideoList" className={styles.twitterTimeLine}>
        <TweetItem
          tweet={twitterTimeline.getIn([this.state.showingTweetIndex])}
        />
        <div className={styles.postSwitcher}>
          <i
            title="up icon"
            className={'icomoon icomoon-chevron-up ' + styles.navigationContent}
            onClick={() => this.navigateTweets(1)}
          />
          <span className={styles.counter}>
            {`${this.state.showingTweetIndex+1}/${this.props.twitterTimeline.size}`}
          </span>
          <i
            title="down icon"
            className={'icomoon icomoon-chevron-down ' + styles.navigationContent}
            onClick={() => this.navigateTweets(-1)}
          />
        </div>
      </div>
    );
  }
}

TwitterTimeLine.propTypes = {
  twitterTimeline: ImmutablePropTypes.list.isRequired,
};

export default TwitterTimeLine;
