import React, { PureComponent } from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { fakeEllipisise } from 'shared/utils/stringUtils';
import { getDayByName, getElapsedTime } from 'shared/utils/timeDateUtils';
import styles from './TweetItem.scss';

class TweetItem extends PureComponent {

  render() {
    const { tweet } = this.props;

    const createDate = new Date(tweet.getIn(['created_at']));

    return (
      <a
        className={styles.tweetItem}
        href={getUrl(tweet.getIn(['id_str']), tweet.getIn(['screen_name']))}>
        <div>
          { renderThumbNail(tweet.getIn(['profile_image_url_https']), styles.profileImg) }
        </div>
        <div className={styles.textContainer}>
          <div className={styles.tweetHeader}>
            <span className={styles.name}>
              {tweet.getIn(['name'])}
            </span>
            <span className={styles.screenName}>
              {`@${tweet.getIn(['screen_name'])}`}
            </span>
            <i className={'icomoon icomoon-twitter ' + styles.twitterIcon} />
          </div>
          <div className={styles.text}>
            {parseTweetText(fakeEllipisise(tweet.getIn(['full_text']), 120))}
            <span className={styles.localeDate}>
              {getElapsedTime(createDate)}
            </span>
          </div>
        </div>
      </a>
    );
  }
}

function parseTweetText(text) {
  // remove mentions in the start if more then one
  let stop = false;
  let filter = '';
  let firstMention = '';
  text.split(' ').forEach((item, index) => {
    if (stop || !item.startsWith('@')) {
      filter += stop ? ' ' + item : item;

      stop = true;
    }
    if (item.startsWith('@') && index === 0) {
      firstMention = item + ' ';
    }
  });

  return firstMention + filter;
}

function getUrl(tweetId, screenName) {
  return `https://twitter.com/${screenName}/status/${tweetId}`;
}

function renderThumbNail(url, className) {
  return (
    <img
      className={className}
      src={url}
      srcSet={`${url} 48w, ${url.replace('_normal', '_bigger')} 73w`}
      width={48}
      height={48}
      alt="youtube video item"
    />
  );
}

TweetItem.propTypes = {
  tweet: ImmutablePropTypes.map,
};

export default TweetItem;
