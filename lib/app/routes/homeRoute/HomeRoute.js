import React, { Component } from 'react';
import { connect } from 'react-redux';
import AM from 'appManager';
import universal from 'react-universal-component';
import { fetchHomeRouteData, fetchAndHandleHomeRouteData } from './homeRouteState';
import CoinTable from './coinTable/CoinTable';
import styles from './HomeRoute.scss';

const CoinRoute = universal(import('routes/coinRoute/CoinRoute'));

class HomeRoute extends Component {
  constructor(props) {
    super(props);
    const isContainerReady = AM.isContainerReady(this, 'HomeRoute');

    this.state = {
      serverHasLoadedData: isContainerReady,
    };
  }

  static fetchData(store) {
    return store.dispatch(fetchAndHandleHomeRouteData('server'));
  }

  componentDidMount() {
    if (!this.props.homeRouteData) {
      const { fetchAndHandleHomeRouteData } = this.props;
      fetchAndHandleHomeRouteData(this.props.location.key);
    }
    CoinRoute.preload();
  }

  componentDidUpdate(prevProps) {
    const hasUrlKeyChanged = prevProps.location.key !== this.props.location.key;
    if (hasUrlKeyChanged) {
      const { fetchAndHandleHomeRouteData } = this.props;
      fetchAndHandleHomeRouteData(this.props.location.key);
    }
  }

  render() {
    return (
      <div className={styles.homeContainer}>
        <div className={`${styles.homeColumn}`}>
          {
            this.props.coinTableData &&
              <CoinTable coinTableData={this.props.coinTableData} />
          }
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    fetchedDataContainerName: state.requestState.get('fetchedDataContainerName'),
    homeRouteData: state.homeRoute.getIn(['homeRouteState', 'homeRouteData']),
    coinTableData: state.homeRoute.getIn(['homeRouteState', 'coinTableData']),
  };
}

export default connect(mapStateToProps, { fetchAndHandleHomeRouteData })(HomeRoute);
