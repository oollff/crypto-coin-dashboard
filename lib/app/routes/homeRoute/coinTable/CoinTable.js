import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import ImmutablePropTypes from 'react-immutable-proptypes';
import classNames from 'classnames/bind';
import RouterLink from 'shared/components/links/routerLink/RouterLink';
import TableHead from './tableHead/TableHead';
import styles from './coinTable.scss';
import TableOptions from './tableOptions/TableOptions';

class CoinRow extends PureComponent {

  getStyledPriceChange(priceChange) {
    const parsedPrice = parseFloat(priceChange).toFixed(2);
    const className = classNames({
      [styles.negativePriceChange]: !(parsedPrice >= 0),
      [styles.positivePriceChange]: parsedPrice >= 0,
    });

    return (
      <td className={className}>
        {`${parsedPrice}%`}
      </td>
    );
  }

  render() {
    const { coin, index } = this.props;

    return (
      <tr className={styles.matchItem}>
        <td>
          {index + 1}
        </td>
        <td>
          <RouterLink to={`/coin/${coin.getIn(['symbol'])}/dashboard`}>
            <i title="github" className={'icomoon icomoon-' + coin.getIn(['symbol']).toLowerCase()} />
            {coin.getIn(['symbol'])}
          </RouterLink>
        </td>
        <td>
          {coin.getIn(['last'])}
        </td>
        {
          this.getStyledPriceChange(coin.getIn(['priceChange']))
        }
        <td>
          n/a
        </td>
        <td>
          n/a
        </td>
        <td>
          n/a
        </td>
        <td>
          n/a
        </td>
        <td>
          n/a
        </td>
        <td>
          n/a
        </td>
      </tr>
    );
  }
}

class CoinListing extends PureComponent {
  render() {
    return (
      [
        this.props.coinTableData.map((coin, index) => {
          return (
            <CoinRow key={coin.get('last')} coin={coin} index={index} />
          );
        })
      ]
    );
  }
}

class CoinTable extends PureComponent {
  render() {
    if (this.props.errorMessage) {
      return (
        <p>{this.props.errorMessage}</p>
      );
    }

    return (
      <div>
        <TableOptions />
        <table className={styles.matchesTable}>
          <TableHead />
          <tbody>
            <CoinListing
              coinTableData={this.props.coinTableData}
            />
          </tbody>
        </table>
      </div>
    );
  }
}

CoinTable.propTypes = {
  coinTableData: ImmutablePropTypes.list,
};

function mapStateToProps(state) {
  return {
    errorMessage: state.homeRoute.getIn(['coinTableState', 'errorMessage']),
  };
}

export default connect(mapStateToProps)(CoinTable);
