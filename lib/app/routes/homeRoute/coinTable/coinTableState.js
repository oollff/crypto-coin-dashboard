import { fromJS } from 'immutable';
import { getCoinPrice } from 'connectivity/api.coinPrice';
import { FETCH_AND_HANDLE_COIN_TABLE } from '../homeRouteState';

let defaultState = fromJS({
  errorMessage: '',
});

export const COIN_TABLE_ERROR = Symbol('COIN_TABLE_ERROR');

export default function(state = defaultState, action) {
  switch (action.type) {
    case COIN_TABLE_ERROR:
      return state.set('errorMessage', action.error);
    default:
      return state;
  }
}

export function fetchAndHandleCoinTable(coinSymbols, baseSymbol, exchange) {
  return function (dispatch) {
    // Create an array of promises inside of promise all to be executed async and collect the data in
    // a single callback when all promises are done. Needs to be wrapped inside Promise.all.
    return Promise.all(coinSymbols.map( coinId => getCoinPrice(baseSymbol, coinId.get('symbol'), exchange)))
      .then((responseArray) => {
        const coinSymbols = responseArray.map(symbol => {
          return symbol.data;
        });

        return dispatch({
          type: FETCH_AND_HANDLE_COIN_TABLE,
          data: coinSymbols,
        });
      }).catch(() => {
        dispatch({
          type: COIN_TABLE_ERROR,
          error: 'ERROR fetching coins for table',
        });
      });
  };
}
