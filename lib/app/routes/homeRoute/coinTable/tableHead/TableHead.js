import React from 'react';
import styles from './TableHead.scss';

function TableHead() {

  return (
    <thead>
      <tr className={styles.headerRow}>
        <th>#</th>
        <th>Name</th>
        <th>$</th>
        <th>%</th>
        <th>
          <i title="whales" className="icomoon icomoon-grooveshark" />
        </th>
        <th>
          <i title="team" className="icomoon icomoon-team" />
        </th>
        <th>
          <i title="github" className="icomoon icomoon-github" />
        </th>
        <th>
          <i title="youtube" className="icomoon icomoon-youtube" />
        </th>
        <th>
          <i title="twitter" className="icomoon icomoon-twitter" />
        </th>
        <th>
          <i title="reddit" className="icomoon icomoon-reddit" />
        </th>
      </tr>
    </thead>
  );
}

export default TableHead;
