import React, { PureComponent } from 'react';
import styles from './TableOptions.scss';

function Options() {

  return (
    <div className={styles.Options}>
      <div>
        <p>Time Period</p>
        <p>----Slider-----</p>
      </div>
      <div>
        <p>Price</p>
        <p>----Slider-----</p>
      </div>
      <div>
        <p>Table Headers</p>
        <span>$</span>
        <span>%</span>
        <span>Team</span>
        <span>Whale</span>
        <span>Youtube</span>
      </div>
    </div>
  );
}

class TableOptions extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showFilters: false,
    };
  }

  render() {
    return (
      <div>
        <div className={styles.tableOptions}>
          <div>
            Active Options
          </div>
          <input
            type="button"
            value="Filters"
            onClick={() => this.setState({ showFilters: !this.state.showFilters })}
          />
        </div>
        {
          this.state.showFilters &&
            <Options />
        }
      </div>
    );
  }
}

export default TableOptions;
