import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { getAllCoinIds } from 'connectivity/api.contentful';
import { getFreshState } from 'shared/utils/routerUtils/routerUtils';
import { fetchAndHandleCoinTable } from './coinTable/coinTableState';

export const FETCH_CONTENTFUL_DATA_SUCCESS = Symbol('FETCH_CONTENTFUL_DATA_SUCCESS');
export const FETCH_AND_HANDLE_COIN_TABLE = Symbol('FETCH_AND_HANDLE_COIN_TABLE');
export const HOME_ROUTE_ERROR = Symbol('HOME_ROUTE_ERROR');

let defaultState = fromJS({
  urlKey: '',
  homeRouteData: null,
  coinTableData: null,
  errorMessage: '',
});

export default function(state = defaultState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      const regex = new RegExp('^/$');

      return getFreshState(state, defaultState, regex, action.payload, state.get('urlKey'));
    case FETCH_CONTENTFUL_DATA_SUCCESS:
      return state.merge({
        urlKey: action.payload.urlKey,
        homeRouteData: action.payload.data,
        errorMessage: '',
      });
    case FETCH_AND_HANDLE_COIN_TABLE:
      return state.merge({ 'coinTableData': action.data }).set('errorMessage', '');
    case HOME_ROUTE_ERROR:
      return state.set('errorMessage', action.error);
    default:
      return state;
  }
}

function homeRouteError(error) {
  return {
    type: HOME_ROUTE_ERROR,
    error: error,
  };
}

export function fetchAndHandleHomeRouteData(urlKey) {
  return function (dispatch, getState) {
    return dispatch(fetchHomeRouteData(urlKey)).then(() => {
      const promises = [];

      const homeRouteData = getState().homeRoute.getIn(['homeRouteState', 'homeRouteData']);
      if (homeRouteData) { promises.push(dispatch(fetchAndHandleCoinTable(homeRouteData, 'BTC', 'BITTREX'))); }

      return Promise.all(promises);
    });
  };
}

function fetchHomeRouteData(urlKey) {
  return function (dispatch) {
    return getAllCoinIds().then((response) => {
      dispatch({
        type: FETCH_CONTENTFUL_DATA_SUCCESS,
        payload: { data: response.data, urlKey: urlKey },
      });
    }).catch(() => {
      dispatch(homeRouteError('Fetching consul key error'));
    });
  };
}
