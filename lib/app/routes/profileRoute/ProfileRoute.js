import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import withAuthorization from 'shared/components/authentication/withAuthorization/withAuthorization';
import styles from './ProfileRoute.scss';

class ProfileRoute extends PureComponent {

  render() {
    return (
      <div className={styles.profileRoute}>
        hi
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
  };
}

const authCondition = (authUser) => !!authUser;

export default compose(
  withAuthorization(authCondition),
  connect(mapStateToProps)
)(ProfileRoute);
