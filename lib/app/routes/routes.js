
import universal from 'react-universal-component';
import Loader from 'shared/components/viewHelper/Loader';
const options = { loading: Loader };

export default [
  {
    path: '/',
    exact: true,
    componentPath: 'routes/homeRoute/HomeRoute',
    Component: universal(import('routes/homeRoute/HomeRoute'), options),
  },
  {
    path: '/coin/:symbol/dashboard',
    exact: true,
    componentPath: 'routes/coinRoute/CoinRoute',
    Component: universal(import('routes/coinRoute/CoinRoute'), options),
  },
  {
    path: '/coin/:symbol/dashboard/expanded',
    exact: true,
    componentPath: 'routes/coinRoute/coinRouteExpanded/CoinRouteExpanded',
    Component: universal(import('routes/coinRoute/coinRouteExpanded/CoinRouteExpanded'), options),
  },
  {
    path: '/profile',
    exact: true,
    componentPath: 'routes/profileRoute/ProfileRoute',
    Component: universal(import('routes/profileRoute/ProfileRoute'), options),
  },
];
