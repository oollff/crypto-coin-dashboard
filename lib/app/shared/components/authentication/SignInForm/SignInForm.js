import React, { Component } from 'react';
import AM from 'appManager';
import { doSignInWithEmailAndPassword } from 'connectivity/api.firebase';
import styles from './SignInForm.scss';

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
};

class SignInForm extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    const { email, password, } = this.state;

    doSignInWithEmailAndPassword(email, password)
      .then(() => {
        this.setState(() => ({ ...INITIAL_STATE }));
        AM.goToURL('/profile' + this.state.coin);
      })
      .catch(error => {
        this.setState(byPropKey('error', error));
      });

    event.preventDefault();
  }

  render() {
    const { email, password, error, } = this.state;

    const isInvalid = (password === '') || (email === '');

    return (
      <form className={styles.signInForm} onSubmit={this.onSubmit}>
        <input
          value={email}
          onChange={event => this.setState(byPropKey('email', event.target.value))}
          type="text"
          placeholder="Email Address"
        />
        <input
          value={password}
          onChange={event => this.setState(byPropKey('password', event.target.value))}
          type="password"
          placeholder="Password"
        />
        <button disabled={isInvalid} type="submit">
          Sign In
        </button>
        {error &&
          <p>{error.message}</p>
        }
      </form>
    );
  }
}

export default SignInForm;

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value,
});
