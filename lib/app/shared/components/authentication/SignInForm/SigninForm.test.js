import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';
import SignInForm from './SignInForm';

describe('SignInForm component snapshot', () => {
  it('should render correctly', () => {
    const component = renderer.create(<SignInForm />).toJSON();
    expect(component).toMatchSnapshot();
  });
});

describe('SignInForm component', () => {
  it('submit button should be disabled on load', () => {
    const wrapper = shallow(<SignInForm />);
    expect(wrapper.find('button')).toHaveLength(1);
    expect(wrapper.find('button').prop('disabled')).toBe(true);
  });

  it('submit button should be disabled if only email is entered', () => {
    const wrapper = shallow(<SignInForm />);
    expect(wrapper.find('button')).toHaveLength(1);
    expect(wrapper.find('button').prop('disabled')).toBe(true);

    wrapper.setState({ email: 'test@cryptodash.io' });
    expect(wrapper.find('button').prop('disabled')).toBe(true);
  });

  it('submit button should not be disabled if email and password has been entered', () => {
    const wrapper = shallow(<SignInForm />);
    expect(wrapper.find('button')).toHaveLength(1);
    expect(wrapper.find('button').prop('disabled')).toBe(true);

    wrapper.setState({ email: 'test@cryptodash.io', password: 'mypassword' });
    expect(wrapper.find('button').prop('disabled')).toBe(false);
  });
});
