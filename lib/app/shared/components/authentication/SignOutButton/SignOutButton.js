import React from 'react';
import { doSignOut } from 'connectivity/api.firebase';
import styles from './SignOutButton.scss';

const SignOutButton = () =>
  <button
    className={styles.signOutButton}
    type="button"
    onClick={doSignOut}>
    Sign Out
  </button>;

export default SignOutButton;
