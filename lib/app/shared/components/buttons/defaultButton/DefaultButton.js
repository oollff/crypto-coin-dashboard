import React from 'react';
import PropTypes from 'prop-types';

import './DefaultButton.scss';

function DefaultButton({ className, label, isActive, onClick }) {

  let defaultButtonClassName = className ?
    `default-button ${className}`
    :
    'default-button';

  defaultButtonClassName = isActive ?
    `${defaultButtonClassName} btn__cta--disabled`
    :
    defaultButtonClassName;

  return (
    <button
      onClick={(e) => onClick(e)}
      className={defaultButtonClassName}>
      {label}
    </button>
  );
}

DefaultButton.propTypes = {
  className: PropTypes.string,
  isActive: PropTypes.bool,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

export default DefaultButton;
