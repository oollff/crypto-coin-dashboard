import React, { PureComponent } from 'react';
import classNames from 'classnames/bind';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Map, List } from 'immutable';
import AM from 'appManager';
import ResizeHelper from 'shared/components/resizeHelper/ResizeHelper';
import CarouselListing from './CarouselListing';
import styles from './Carousel.scss';

const SNAP_THRESHOLD = 0.3;

class Carousel extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      breakpointIndex: 0,
      clickDownXPos: 0,
      clickDownYPos: 0,
      dragging: false,
      elementWidth: 0,
      index: 0,
      scrolling: false,
      xPos: 0,
    };

    this.setListRef = this.setListRef.bind(this);
    this.onTouchEnd = this.onTouchEnd.bind(this);
    this.onTouchStart = this.onTouchStart.bind(this);
    this.onTouchMove = this.onTouchMove.bind(this);
    this.slideToNext = this.slideToNext.bind(this);
    this.slideToPrevious = this.slideToPrevious.bind(this);
    this.onResize = this.onResize.bind(this);
  }

  componentDidMount() {
    this.updatePosition();
  }

  componentDidUpdate(prevProps, prevState) {
    /* Since the size is calculated before the component is rendered we need to make an extra check
        if the breakpoint index have just changed, since this results in a major size difference. */
    if (prevState.breakpointIndex !== this.state.breakpointIndex) {
      this.updatePosition();
    }
  }

  updatePosition() {
    if (!this.listRef) {
      return;
    }
    const element = this.getListRefChildAtIndex(0);

    const newElementWidth = element ? element.offsetWidth : 0;
    const newBreakpointIndex = this.getCurrentBreakPointIndex();
    const newIndex = this.getIndexInBound(this.state.index);

    this.setState({
      breakpointIndex: newBreakpointIndex,
      elementWidth: newElementWidth,
      index: newIndex,
    });
  }

  getCurrentBreakPointIndex() {
    const responsive = this.props.settings.get('responsive');
    if (!responsive) {
      return 0;
    }

    let newBreakpointIndex = 0;
    for (let i = 0; i < responsive.size; i++) {
      if (responsive.get(i).get('breakpoint') > window.innerWidth) {
        break;
      }
      newBreakpointIndex = i + 1;
    }

    return newBreakpointIndex;
  }

  slideToNext() {
    // Get next index
    const lastDotIndex = this.state.index + this.getProp('nbrOfSlidesInView') - 1;
    const nextIndex = (lastDotIndex >= this.getChildrenLength() - 1) ? 0 : (this.state.index + 1);

    this.setState({
      index: nextIndex,
    });
  }

  slideToPrevious() {
    // Get previous index
    const previousIndex = (this.state.index <= 0) ?
      this.getChildrenLength() - this.getProp('nbrOfSlidesInView')
      :
      this.state.index - 1;

    this.setState({
      index: previousIndex,
    });
  }

  snapTo(amountMoved, elementWidth) {
    const slidesMoved = this.getSlidesMoved(amountMoved, elementWidth);

    if ( amountMoved > 0) {
      return slidesMoved;
    } else {
      return -slidesMoved;
    }
  }

  getSlidesMoved(amountMoved, elementWidth) {
    const slidesMoved = Math.abs(amountMoved / elementWidth);
    const wholeSlidesMoved = Math.floor(slidesMoved);

    if (slidesMoved % 1 > SNAP_THRESHOLD) {
      return wholeSlidesMoved + 1;
    }

    return wholeSlidesMoved;
  }

  // We need this to support both immutable and general lists
  getChildrenLength() {
    if (List.isList(this.props.children)) {
      return this.props.children.size;
    } else {
      return this.props.children.length;
    }
  }

  getListRefChildAtIndex(index) {
    if (List.isList(this.listRef.children)) {
      return this.listRef.children.get(index);
    } else {
      return this.listRef.children[index];
    }
  }

  slidesMoved(newXPos) {
    const elementWidth = this.state.elementWidth;
    const amountMoved = newXPos - elementWidth * this.state.index;
    const slidesMoved = this.snapTo(amountMoved, elementWidth);

    return slidesMoved;
  }

  // Prevents index from being negative or too big
  getIndexInBound(index) {
    if (index < 0) {
      return 0;
    } else if (index + this.getProp('nbrOfSlidesInView') > this.getChildrenLength() - 1) {
      return this.getChildrenLength() - this.getProp('nbrOfSlidesInView');
    } else {
      return index;
    }
  }

  // Default props are the top level props
  isDefaultProp(prop) {
    const isDefaultBreakpoint = this.state.breakpointIndex === 0;
    if (isDefaultBreakpoint) {
      return true;
    }

    const nonResponsive = !this.props.settings.get('responsive');
    if (nonResponsive) {
      return true;
    }

    const breakpointListIndex = this.state.breakpointIndex - 1;
    const hasNotResponsiveProp = !this.props.settings.getIn(['responsive', breakpointListIndex, 'settings', prop]);
    if (hasNotResponsiveProp) {
      return true;
    }

    return false;
  }

  // Returns correct prop according to screen size
  getProp(prop) {
    if (this.isDefaultProp(prop)) {
      // Returns default prop
      return this.props.settings.get(prop);
    } else {
      const breakpointIndex = this.state.breakpointIndex - 1;

      return AM.val(this.props.settings, 'responsive', breakpointIndex, 'settings', prop);
    }
  }

  getImageFlexBase() {
    const nbrOfSlidesInView = this.getProp('nbrOfSlidesInView');
    if (nbrOfSlidesInView) {
      const flexBasis = 100/nbrOfSlidesInView;

      return `0 0 ${flexBasis}%`;
    }
  }

  getDotClassName(index) {
    const dotActiveCondition = index >= this.state.index &&
            index < this.state.index + this.getProp('nbrOfSlidesInView');

    return classNames({
      [styles.dotActive]: dotActiveCondition,
      [styles.dot]: !dotActiveCondition,
    });
  }

  showArrows() {
    return this.getProp('showArrows') && (this.getChildrenLength() > this.getProp('nbrOfSlidesInView'));
  }

  setListRef(item) {
    this.listRef = item;
  }

  getMostItemsPossibleInView() {
    let maxNbrOfItemsInView = this.props.settings.get('nbrOfSlidesInView');

    if (this.props.settings.get('responsive')) {
      maxNbrOfItemsInView = this.props.settings.get('responsive').reduce((max, current) => {
        const currentSlidesInView = AM.val(current, 'settings', 'nbrOfSlidesInView');

        return  currentSlidesInView > max ? currentSlidesInView : max;
      }, maxNbrOfItemsInView || 0);
    }

    return maxNbrOfItemsInView;
  }

  handleDotClick(index) {
    this.setState({
      index: index,
    });
  }

  onTouchStart(e) {
    const xPos = e.changedTouches[0].pageX;
    const yPos = e.changedTouches[0].pageY;
    if (this.listRef.contains(e.target)) {
      this.setState({
        clickDownXPos: xPos,
        clickDownYPos: yPos,
        dragging: true,
      });
    }
  }

  onTouchMove(e) {
    if (!this.state.scrolling) {
      const xPos = e.changedTouches[0].pageX;
      const yPos = e.changedTouches[0].pageY;
      const amountMoved = this.state.clickDownXPos - xPos;
      const amountScrolled = this.state.clickDownYPos - yPos;

      if (this.state.xPos === 0 &&
                Math.abs(amountMoved) < 5 &&
                Math.abs(amountMoved) < Math.abs(amountScrolled)) {
        this.setState({
          scrolling: true,
        });
        this.enableScroll();
      }
      else {
        if (this.state.xPos === 0) {
          this.disableScroll();
        }
        this.setState({
          xPos: amountMoved,
        });
      }
    }

  }

  onTouchEnd(e) {
    if (this.state.scrolling) {
      this.setState({
        scrolling: false,
        dragging: false,
      });
    }
    else if (this.listRef.contains(e.target)) {
      const xPos = e.changedTouches[0].pageX;
      const newXPos = this.state.index * this.state.elementWidth + this.state.clickDownXPos - xPos;

      const newIndex = this.state.index + this.slidesMoved(newXPos);

      this.setState({
        dragging: false,
        index: this.getIndexInBound(newIndex),
        xPos: 0,
      });
      this.enableScroll();
    }
  }

  onResize() {
    this.updatePosition();
  }

  noScrollHandler(e) {
    e.preventDefault();
  }

  disableScroll() {
    document.addEventListener('touchmove', this.noScrollHandler, { passive: false });
  }

  enableScroll() {
    document.removeEventListener('touchmove', this.noScrollHandler, { passive: false });
  }

  render() {
    const { className, children, lazyLoad } = this.props;

    const elementWidth = this.state.elementWidth;
    const xScroll = elementWidth * this.state.index;
    const listStyle = {
      transform: `translate3d(${-xScroll - this.state.xPos}px, 0, 0)`,
      transition: this.state.dragging ? '' : 'transform 200ms',
      minHeight: '100%',
    };

    return (
      <div className={className} style={{ minHeight: '100%' }}>
        <ResizeHelper onResize={this.onResize} />
        <div
          className={styles.slider}
          onTouchStart={this.onTouchStart}
          onTouchMove={this.state.dragging ? this.onTouchMove : null}
          onTouchEnd={this.onTouchEnd}
          onTouchCancel={this.state.dragging ? this.onTouchEnd : null}>
          <CarouselListing
            listStyle={listStyle}
            maxNbrOfItemsInView={this.getMostItemsPossibleInView()}
            itemStyle={{ flex: this.getImageFlexBase() }}
            inputRef={this.setListRef}
            items={children}
            lazyLoad={lazyLoad}
          />
          {
            this.showArrows() &&
              [
                <div
                  key="left"
                  className={styles.arrowLeft}
                  onClick={this.slideToPrevious}>
                  <i className={`${styles.iconArrow} icon-arrow-left`} />
                </div>,
                <div
                  key="right"
                  className={styles.arrowRight}
                  onClick={this.slideToNext}>
                  <i className={`${styles.iconArrow} icon-arrow-right`} />
                </div>,
              ]
          }
        </div>
        {
          this.getProp('showDots') &&
            <div className={styles.dots}>
              {
                children.map((kid, index) => {
                  return (
                    <div
                      className={this.getDotClassName(index)}
                      onClick={() => this.handleDotClick(index)}
                      key={index}
                    />
                  );
                })
              }
            </div>
        }
      </div>
    );
  }
}

Carousel.defaultProps = {
  children: List(), // don't crash if there are no slides
  settings: Map({
    nbrOfSlidesInView: 1,
    showArrows: true,
    showDots: true,
  }),
  lazyLoad: false,
};

Carousel.propTypes = {
  children: ImmutablePropTypes.list,
  className: PropTypes.string,
  settings: ImmutablePropTypes.mapContains({
    nbrOfSlidesInView: PropTypes.number.isRequired,
    showArrows: PropTypes.bool,
    showDots: PropTypes.bool,
    responsive: ImmutablePropTypes.list,
  }),
  lazyLoad: PropTypes.bool,
};

export default Carousel;
