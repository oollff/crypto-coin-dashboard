import React from 'react';
import { shallow } from 'enzyme';
import { List, Map } from 'immutable';
import * as ReactDOM from 'react-dom';
import { createMoveTouchEventObject } from '../../../__helpers__/EventHelpers';
import Carousel from '../Carousel';
import CarouselListing from '../CarouselListing';

const mockChild = List([
  <div key={1}>first slide</div>,
  <div key={2}>second slide</div>,
  <div key={3}>third slide</div>,
  <div key={4}>fourth slide</div>,
]);

const setup = propOverrides => {
  const props = Object.assign({
    settings: undefined,
  }, propOverrides);

  const wrapper = shallow(<Carousel {...props}>{ mockChild }</Carousel>);
  const slider = wrapper.find('.slider');

  return {
    props,
    wrapper,
    slider,
  };
};

describe('rendering', () => {
  describe('smoke test', () => {
    it('should match snapshot', () => {
      const { wrapper } = setup();

      expect(wrapper).toMatchSnapshot();
    });

    it('should render component', () => {
      const props = {
        settings: undefined,
      };
      const div = document.createElement('div');
      ReactDOM.render(<Carousel {...props} />, div);
    });
  });

  describe('Renders props', () => {
    it('has className', () => {
      const className = 'some-test-class';
      const { wrapper } = setup({
        className,
      });

      expect(wrapper.hasClass('some-test-class')).toEqual(true);
    });

    it('can specify how many slides are in view', () => {
      const { slider } = setup({
        settings: Map({
          nbrOfSlidesInView: 2,
        }),
      });

      // Get one of the elements in view
      const slidesInView = slider.find(CarouselListing).dive()
        .find('.list').children().get(0).props.style;

      // Flexbox decides how many are in view
      const style = { flex: '0 0 50%' };
      expect(slidesInView).toEqual(style);
    });

    describe('showArrows', () => {
      it('show showArrows by default', () => {
        const { slider } = setup();
        expect(slider.find('.iconArrow').length).toBe(2);
      });

      it('can hide arrows', () => {
        const { slider } = setup({
          settings: Map({
            showArrows: false,
            nbrOfSlidesInView: 1,
          }),
        });
        expect(slider.find('.iconArrow').length).toBe(0);
      });

      it('should hide arrows if nbrOfSlidesInView is bigger then children', () => {
        const { slider } = setup({
          settings: Map({
            showArrows: true,
            nbrOfSlidesInView: mockChild.size,
          }),
        });
        expect(slider.find('.iconArrow').length).toBe(0);
      });

      it('should show arrows if nbrOfSlidesInView is less then children', () => {
        const { slider } = setup({
          settings: Map({
            showArrows: true,
            nbrOfSlidesInView: mockChild.size - 1,
          }),
        });
        expect(slider.find('.iconArrow').length).toBe(2);
      });
    });

    it('should show dots', () => {
      const { wrapper } = setup({
        settings: Map({
          showDots: true,
          nbrOfSlidesInView: 1,
        }),
      });

      expect(wrapper.find('.dots').length).toBe(1);
    });

    it('should lazyLoad items that are not in view', () => {
      const { slider } = setup({
        lazyLoad: true,
        settings: Map({
          nbrOfSlidesInView: 2,
        }),
      });

      const itemsInView = slider.find(CarouselListing).dive()
        .find('.list')
        .children().filterWhere(n => n.prop('lazyLoad') === false);
      expect(itemsInView.length).toBe(2);
    });
  });

  describe('touch events', () => {
    const { wrapper } = setup({
      settings: Map({
        nbrOfSlidesInView: 1,
      }),
      children: ['child', 'child', 'child'],
    });

    wrapper.setState({ elementWidth: 100 });
    wrapper.instance().listRef = { contains: () => true };

    const slider = wrapper.find('.slider');

    it('swipe left should change to next slide', () => {
      wrapper.setState({ index: 0 });

      slider.simulate('touchStart', createMoveTouchEventObject({ x: 50, y: 0 }));
      slider.simulate('touchMove', createMoveTouchEventObject({ x: 0, y: 0 }));
      slider.simulate('touchEnd', createMoveTouchEventObject({ x: 0, y: 0 }));

      expect(wrapper.state('index')).toBe(1);
    });

    it('swipe right should change to prev slide', () => {
      wrapper.setState({ index: 1 });

      slider.simulate('touchStart', createMoveTouchEventObject({ x: 0, y: 0 }));
      slider.simulate('touchMove', createMoveTouchEventObject({ x: 50, y: 0 }));
      slider.simulate('touchEnd', createMoveTouchEventObject({ x: 50, y: 0 }));

      expect(wrapper.state('index')).toBe(0);
    });
  });
});
