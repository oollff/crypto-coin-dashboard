import React from 'react';
import * as ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import CarouselItem from '../CarouselItem';

const setup = propOverrides => {
  const props = Object.assign({
    style: undefined,
    item: undefined,
    lazyLoad: undefined,
  }, propOverrides);

  const wrapper = shallow(<CarouselItem {...props} />);

  return {
    props,
    wrapper,
  };
};

describe('rendering', () => {
  describe('smoke test', () => {
    it('should match snapshot', () => {
      const { wrapper } = setup();
      expect(wrapper).toMatchSnapshot();
    });

    it('should render component', () => {
      const div = document.createElement('div');
      ReactDOM.render(<CarouselItem />, div);
    });
  });
});
