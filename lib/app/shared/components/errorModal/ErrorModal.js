import React from 'react';
import PropTypes from 'prop-types';

import './ErrorModal.scss';

function ErrorModal({ errorMessage, closeModal }) {
  return (
    <div className={'error-modal-container'}>
      <div className={'error-modal-container__message'}>
        {errorMessage}
      </div>
      <div
        className={'error-modal-container__overlay'}
        onClick={closeModal}
      />
    </div>
  );
}

ErrorModal.propTypes = {
  errorMessage: PropTypes.string,
  closeModal: PropTypes.func,
};

export default ErrorModal;
