import React, { PureComponent } from 'react';

class BasicLink extends PureComponent {
  render() {
    const { to, ...rest } = this.props;

    return (
      <a href={to || '/'} {...rest} />
    );
  }
}

export default BasicLink;
