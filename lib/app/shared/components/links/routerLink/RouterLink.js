import React, { PureComponent } from 'react';
import { Link as ReactRouterLink } from 'react-router-dom';
import { treatUrl } from 'shared/utils/uriUtils';

class RouterLink extends PureComponent {
  render() {
    const { to, state, ...rest } = this.props;
    const url = treatUrl(to);

    return (
      <ReactRouterLink
        to={{
          pathname: url || '/',
          state: state,
        }}
        {...rest}
      />
    );
  }
}

export default RouterLink;
