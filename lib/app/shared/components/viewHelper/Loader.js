import React from 'react';
import PropTypes from 'prop-types';

function Loader({ className }) {
  return (
    <div className={'loader ' + (className ? className : '')}>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
      <span></span>
    </div>
  );
}

Loader.propTypes = {
  className: PropTypes.string
};

export default Loader;
