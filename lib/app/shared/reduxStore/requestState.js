import Immutable from 'immutable';

export const SET_REQUEST_STATE = Symbol('SET_REQUEST_STATE');
export const SET_FETCHED_CONTAINER_NAME = Symbol('SET_FETCHED_CONTAINER_NAME');
export const SET_X_REQUEST_TOKEN = Symbol('SET_X_REQUEST_TOKEN');

let defaultState = Immutable.fromJS({
  serverRequestInfo: {},
  xRequestToken: null,
  fetchedDataContainerName: []
});

export default function(state = defaultState, action) {
  switch (action.type) {
    case SET_REQUEST_STATE:
      return state.merge({ serverRequestInfo: action.serverRequestInfo });

    case SET_X_REQUEST_TOKEN:
      return state.merge(action.xRequestToken);

    case SET_FETCHED_CONTAINER_NAME:
      return state.update('fetchedDataContainerName', (fetchedDataContainerName) => {
        return fetchedDataContainerName.push(action.fetchedDataContainerName);
      });

    default:
      return state;
  }
}

export function setServerRequestInfo(serverRequestInfo) {
  return {
    type: SET_REQUEST_STATE,
    serverRequestInfo
  };
}

export function setFetchedContainerName(fetchedDataContainerName) {
  return {
    type: SET_FETCHED_CONTAINER_NAME,
    fetchedDataContainerName
  };
}

export function setXRequestToken(xRequestToken) {
  return {
    type: SET_X_REQUEST_TOKEN,
    xRequestToken
  };
}
