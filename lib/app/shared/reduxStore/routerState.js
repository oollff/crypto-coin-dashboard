import Immutable from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { getMatchedRouteParams } from 'shared/utils/routerUtils/routerUtils';
import routes from 'routes/routes';

export const SET_INITIAL_ROUTE = Symbol('SET_INITIAL_ROUTE');

let defaultState = Immutable.fromJS({
  location: null,
  params: null
});

export default function (state = defaultState, action) {
  switch (action.type) {
    case LOCATION_CHANGE:
      return state.set('location', Immutable.fromJS(action.payload))
        .set('params', Immutable.fromJS(getMatchedRouteParams(action.payload.pathname, routes)));
    case SET_INITIAL_ROUTE:
      return state.set('location', Immutable.fromJS(action.initialRoute))
        .set('params', Immutable.fromJS(getMatchedRouteParams(action.initialRoute.pathname, routes)));
    default:
      return state;
  }
}

export function setInitialRoute(initialRoute) {
  return {
    type: SET_INITIAL_ROUTE,
    initialRoute
  };
}
