import Immutable from 'immutable';

export const SET_AUTH_USER = Symbol('SET_AUTH_USER');

let defaultState = Immutable.fromJS({
  authUser: null,
});

export default function (state = defaultState, action) {
  switch (action.type) {
    case SET_AUTH_USER:
      return state.set('authUser', action.authUser);
    default:
      return state;
  }
}

export function setAuthUser(authUser) {
  return {
    type: SET_AUTH_USER,
    authUser
  };
}
