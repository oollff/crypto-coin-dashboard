import Immutable from 'immutable';

let defaultState = Immutable.fromJS({});

export const LOADED_CONSUL_SETTINGS = Symbol('LOADED_CONSUL_SETTINGS');

export default function(state = defaultState, action) {
  switch (action.type) {
    case LOADED_CONSUL_SETTINGS:
      return state.merge(action.siteSettings);
    default:
      return state;
  }
}

export function loadConsulSettings(siteSettings) {
  return {
    type: LOADED_CONSUL_SETTINGS,
    siteSettings: siteSettings,
  };
}
