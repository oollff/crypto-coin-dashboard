export function apiDelay(someThis, time, api) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
    }, time);
  }).then(() => {
    someThis.setState(api);
  });
}
