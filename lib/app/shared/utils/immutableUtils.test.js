import { fromJS } from 'immutable';
import { sortListAlphabetically, setToList, setToSortedList } from 'shared/utils/immutableUtils';

describe('Immutable Utils', () => {

  describe('sortListAlphabetically', () => {
    it('should sort items alphabetically given object sort prop', () => {
      const mixedList = fromJS([
        {
          Key: 'klm',
          Value: 'value'
        },
        {
          Key: 'abc',
          Value: 'value'
        },
        {
          Key: 'def',
          Value: 'value'
        },
      ]);

      const sortedListExpected = fromJS([
        {
          Key: 'abc',
          Value: 'value'
        },
        {
          Key: 'def',
          Value: 'value'
        },
        {
          Key: 'klm',
          Value: 'value'
        },
      ]);
      const sortedList = sortListAlphabetically(mixedList, 'Key');
      expect(sortedList).toEqual(sortedListExpected);
    });
  });

  describe('setToList', () => {
    it('Should add item if it doesnt exists', () => {
      const baseList = fromJS([
        {
          Key: 'klm',
          Value: 'value'
        },
        {
          Key: 'abc',
          Value: 'value'
        },
        {
          Key: 'def',
          Value: 'value'
        },
      ]);

      const objToAdd = fromJS(
        {
          Key: 'newKey',
          Value: 'value'
        }
      );

      const addedList = setToList(baseList, 'Key', objToAdd);

      const expectedList = fromJS([
        {
          Key: 'klm',
          Value: 'value'
        },
        {
          Key: 'abc',
          Value: 'value'
        },
        {
          Key: 'def',
          Value: 'value'
        },
        {
          Key: 'newKey',
          Value: 'value'
        }
      ]);
      expect(addedList).toEqual(expectedList);
    });

    it('Should update existing item if it exists', () => {
      const baseList = fromJS([
        {
          Key: 'klm',
          Value: 'value'
        },
        {
          Key: 'abc',
          Value: 'value'
        },
        {
          Key: 'def',
          Value: 'value'
        },
      ]);
      const objToAdd = fromJS(
        {
          Key: 'abc',
          Value: 'changed'
        }
      );
      const addedList = setToList(baseList, 'Key', objToAdd);
      const expectedList = fromJS([
        {
          Key: 'klm',
          Value: 'value'
        },
        {
          Key: 'abc',
          Value: 'changed'
        },
        {
          Key: 'def',
          Value: 'value'
        },
      ]);
      expect(addedList).toEqual(expectedList);
    });
  });

  describe('setToSortedList', () => {
    it('Should add item to list and sort the list alphabetically', () => {
      const baseList = fromJS([
        {
          Key: 'klm',
          Value: 'value'
        },
        {
          Key: 'abc',
          Value: 'value'
        },
        {
          Key: 'def',
          Value: 'value'
        },
      ]);
      const objToAdd = fromJS(
        {
          Key: 'newKey',
          Value: 'changed'
        }
      );
      const addedList = setToSortedList(baseList, 'Key', objToAdd);
      const expectedList = fromJS([
        {
          Key: 'abc',
          Value: 'value'
        },
        {
          Key: 'def',
          Value: 'value'
        },
        {
          Key: 'klm',
          Value: 'value'
        },
        {
          Key: 'newKey',
          Value: 'changed'
        }
      ]);

      expect(addedList).toEqual(expectedList);
    });
  });

});
