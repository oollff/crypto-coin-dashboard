import React from 'react';
import { Route } from 'react-router-dom';

export function hasNavigated(nextRouterState, thisPropsRouterState) {
  return nextRouterState.getIn(['location', 'pathname'])
    !== thisPropsRouterState.getIn(['location', 'pathname']);
}

export function routeWithSubRoutes(route, index) {
  return (
    <Route
      key={'route-'+index}
      exact={route.exact || false}
      status={route.status || false}
      path={route.path}
      render={props => {
        if (props.staticContext) {
          props.staticContext.status = route.status;
        }
        // remove component property since we dont need it in front-end.
        const {
          Component,
          ...routeProperties
        } = route;

        // Pass the sub-routes down to keep nesting
        return <Component {...props} routes={route.routes} route={routeProperties} />;
      }}
    />
  );
}

function cleanTrailing(char, str) {
  return str.endsWith(char) ? str.slice(0, -1) : str;
}

export function getMatchedRouteParams(pathname, routes) {
  const match = routes.find(route => {
    return route.path.split('/')[1] === pathname.split('/')[1];
  });

  if (match && match.path) {
    const path = match.path;

    const paramsStartAt = path.indexOf('/:');
    const pathNameWithoutParams = cleanTrailing('/', path.slice(0, paramsStartAt)).split('/').length;

    const pathnames = cleanTrailing('/', pathname).split('/');

    if (pathNameWithoutParams < pathnames.length) {
      const routeParamsKeys = path.slice(paramsStartAt + 1).split('/');

      const routeParamsValues = pathnames.slice(pathNameWithoutParams);

      const matchParams = routeParamsValues.reduce((accumulatedObject, currentItem, index) => {
        const currentParamKey = routeParamsKeys[index]
          ? routeParamsKeys[index].replace(/[^a-zA-Z ]/g, '')
          : index;

        accumulatedObject[currentParamKey] = currentItem;

        return accumulatedObject;
      }, {});

      return matchParams;
    }
  }
}

export function getPathAfter(pathname, pathbefore) {
  const paths = pathname.split('/');
  const indexOfCoin = paths.findIndex((item) => {

    return item === pathbefore;
  });

  return paths[indexOfCoin + 1];
}

// This function has two purposes.
// 1. Prevent server client double fetching
// 2. Resets state only if NEW url key, back button will not reset state.
export function getFreshState(state, defaultState, reg, routerReduxPayload, currentStateUrlKey) {
  const currentPathName = routerReduxPayload.pathname;

  if (reg.test(currentPathName)) {
    if (currentStateUrlKey === 'server') {
      return state.set('urlKey', routerReduxPayload.key);
    }

    const newUrlKey = routerReduxPayload.key !== currentStateUrlKey;
    if (newUrlKey) {
      return defaultState;
    }
  }

  return state;
}

/*
 * Substitute for PureComponent for components that subscribe to the router state.
 * Necessary to do until this https://github.com/ReactTraining/react-router/issues/6049 has been resolved.
 * @param domain = The route that the component acts on. For example, if it should only update on `/coin` route
 */
export function shouldRouteComponentUpdate(domain, thisProps, nextProps, exact, thisState, nextState) {
  // Router Specifics
  if (exact) {
    if (!(nextProps.routerState.getIn(['location', 'pathname']) === domain)) {
      return false;
    }
  } else {
    if (!(nextProps.routerState.getIn(['location', 'pathname']).startsWith(domain))) {
      return false;
    }
  }

  // Component Specifics
  return (thisProps !== nextProps) || (thisState !== nextState);
}
