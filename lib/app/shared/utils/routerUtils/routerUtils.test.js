import { getPathAfter, getMatchedRouteParams } from './routerUtils';

describe('router Utils', () => {
  describe('getCoinName', () => {
    it('Should extract text after /coin/ string', () => {
      const pathname = 'http://se.pr.com:4006/coin/TenX';
      expect(getPathAfter(pathname, 'coin')).toEqual('TenX');
    });
  });

  describe('getMatchedRouteParams', () => {
    it('Should handle multiple parameters', () => {
      const routes = [
        {
          path: '/',
          exact: true,
          componentPath: 'routes/home/HomeContainer',
        },
        {
          path: '/consul/:folderKey*',
          exact: true,
          componentPath: 'routes/consul/ConsulRoute',
        },
        {
          path: '/offer-au/:tab?/:tabParam?',
          componentPath: 'routes/offerAu/OfferAuRoute',
        },
      ];

      const pathname = '/offer-au/matches/4257585';

      const result = getMatchedRouteParams(pathname, routes);

      const expected = {
        tab: 'matches',
        tabParam: '4257585',
      };

      expect(result).toEqual(expected);
    });

    it('Should return handle only :', () => {
      const routes = [
        {
          path: '/',
          exact: true,
          componentPath: 'routes/home/HomeContainer',
        },
        {
          path: '/coin/:symbol',
          exact: true,
          componentPath: 'routes/consul/ConsulRoute',
        },
        {
          path: '/home/:tab?/:tabParam?',
          componentPath: 'routes/offerAu/OfferAuRoute',
        },
      ];

      const pathname = '/coin/eth';

      const result = getMatchedRouteParams(pathname, routes);

      const expected = {
        symbol: 'eth',
      };

      expect(result).toEqual(expected);
    });
  });
});
