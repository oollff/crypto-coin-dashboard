/*
 * Storage (localStorage, cookies, ....) related utility functions.
 */

// used to know on which "app" the user is - to be used with storage functions
export const APP_NAME_KEY = 'app_name';
export const APP_NAME = 'pr_spa';
export const MS_IN_ONE_YEAR = 31556952000;

export const DEFAULT_VALUE_IF_NO_AVAILABLE_LOCAL_STORAGE = '';
const DEFAULT_VALUE_IF_NO_AVAILABLE_COOKIES = '';

// --- browserStorage functions --- //

export const isLocalStorageAvailable = () => {
  if (typeof window === 'object' && typeof localStorage === 'object') {
    try {
      localStorage.setItem('testKey', 'test');
      localStorage.removeItem('testKey');

      return true;
    }
    catch (error) {
      return false;
    }
  } else {
    return false;
  }
};

export const isSessionStorageAvailable = () => {
  if (typeof window === 'object' && typeof sessionStorage === 'object') {
    try {
      sessionStorage.setItem('testKey', 'test');
      sessionStorage.removeItem('testKey');

      return true;
    }
    catch (error) {
      return false;
    }
  } else {
    return false;
  }
};

// --- cookie functions --- IMPORTANT: use withCookies HOC if possible instead.
// These functions are just when needing access to cookies outside of the app.
// Currently only in configureStore to read the pr_devtools.
// --- //

export const areCookiesAvailable = () => {
  // only allow on client side
  return typeof window === 'object';
};

export const getFromCookies = (key) => {
  if (!areCookiesAvailable()) {
    return DEFAULT_VALUE_IF_NO_AVAILABLE_COOKIES;
  }

  return getCookie(key);
};

export const setToCookies = (key, value, options) => {
  if (areCookiesAvailable()) {
    setCookie(key, value, options);
  }
};

export const removeFromCookies = (key) => {
  if (areCookiesAvailable()) {
    document.cookie = `${key}=; expires=Thu, 01 Jan 1970 00:00:00 UTC;`;
  }
};

export const writeRawCookie = (name, value, expire, path) => {
  if (areCookiesAvailable()) {
    document.cookie = name + '=' + value + (expire ? ';expires='+expire : '') +';path=' + path;
  }
};

function setCookie(cname, cvalue, exdays) {
  let d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = 'expires='+ d.toUTCString();
  document.cookie = cname + '=' + cvalue + ';' + expires + ';path=/';
}

function getCookie(cname) {
  let name = cname + '=';
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }

  return '';
}

export const getValueFromParams = (params, propName) => {
  for (let i = 0; i < params.length; i++) {
    if ( (propName in params[i])) {
      return params[i][propName];
    }
  }
};

export const updateValueToCookie = (params, propName, newValue) => {
  for (let i = 0; i < params.length; i++) {
    if ( (propName in params[i])) {
      params[i][propName] = newValue;

      return newValue;
    }
  }
};
