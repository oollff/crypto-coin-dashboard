/*
 * String related utility functions.
 */

import AM from 'appManager';

export const isInt = (x) => {
  const y = parseInt(x, 10);
  // eslint-disable-next-line
    return !isNaN(y) && (x == y) && (x.toString() == y.toString());
};

export const isPositiveInt = (x) => {
  return isInt(x) && (x > 0);
};

// Truncates a string and replace the end with '...'.
// We do this since not all browsers support multiline ellipsis.
export const fakeEllipisise = (text, maxCharNumber) => {
  if (typeof text === 'string' && text.length > maxCharNumber) {
    return text.substring(0, maxCharNumber - 2).trim() + '…';
  } else {
    return text;
  }
};

export const truncateMiddle = (string, stringLength)  => {
  if (!string || string.length <= stringLength) return string;
  const separator = '... ';

  const sepLen = separator.length,
    charsToShow = stringLength - sepLen,
    frontChars = Math.ceil(charsToShow/2),
    backChars = Math.floor(charsToShow/2);

  return string.substr(0, frontChars) +
        separator +
        string.substr(string.length - backChars);
};

export const replaceAll = (search, replace, str) => {
  return str.split(search).join(replace);
};

export const addDelimiterSpacesToInteger = (n) => {
  // Adds whitespace for better readability
  // e.g.: 1234000 -> 1 234 000
  return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
};

export const scrubHTMLString = (string) => {
  if (!string) {
    return null;
  }

  return string.replace(/<(?:.|\n)*?>/gm, '');
};

export const scrubAllCodeString = (string) => {
  if (!string) {
    return null;
  }

  return string.replace(/<([\S\s]*?)>([\S\s]*?)<\/([\S\s]*?)>/ig, '');
};

// Check if string contains HTML, if so we might want to use dangerouslySetHtml.
export const isHTML = (string) => {
  if (!AM.isClient() || !string) {
    return false;
  }

  const a = document.createElement('div');
  a.innerHTML = string;
  for (let c = a.childNodes, i = c.length; i--; ) {
    if (c[i].nodeType === 1) return true;
  }

  return false;
};

// Check for presence of HTML elements using regex. Can safely be used by the server and client,
export const regexIsHTML = (string) => {
  return /<[a-z][\s\S]*>/i.test(string);
};

export const insertTaggedVariable = (string, tag, replacementString) => {
  // Used to replace tags in translation strings
  return replaceAll('${' + tag + '}', replacementString, string);
};

export const escapeHtml = (text) => {
  const map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    '\'': '&#039;'
  };

  return text.replace(/[&<>"']/g, (m) => map[m]);
};

// Uppercase first letter of string
export const capitalizeFirstChar = (string) => {
  return string[0].toUpperCase() + string.slice(1);
};
