import moment from 'moment';

export function formatDate(date, format, contentLanguageCode) {
  if (contentLanguageCode) {
    moment.locale(contentLanguageCode);
  }

  return moment(date).format(format);
}

export function getElapsedTime(date) {
  const timeElapsed = moment(date);

  return timeElapsed.fromNow();
}

export const isWeekday = (dateObject) => {
  const day = dateObject.getDay();
  if ((day > 0) || (day < 7)) {
    return true;
  } else {
    return false;
  }
};

export const isSaturday = (dateObject) => {
  const day = dateObject.getDay();

  return (day === 6);
};

export const isFriday = (dateObject) => {
  const day = dateObject.getDay();

  return (day === 5);
};

export const isTimeBeforeMidday = (dateObject) => {
  const hour = dateObject.getHours();
  const minutes = dateObject.getMinutes();

  if ((hour < 12) || (hour === 12 && minutes < 25)) {
    return true;
  } else {
    return false;
  }
};

export function getDayByName(date) {
  return moment(date).format('dddd');
}
