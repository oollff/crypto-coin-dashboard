/*
 * URI / URL related utility functions.
 */

export const treatUrl = (url) => {
  return url && url.replace(/&amp;/g, '&');
};

export const treatAndGetUrlWithInfo = (url) => {
  url = treatUrl(url);

  return url;
};

export function getUrlParameters(urlQuery) {
  if (!urlQuery) {
    return {};
  }

  return (/^[?#]/.test(urlQuery) ? urlQuery.slice(1) : urlQuery).split('&').reduce((params, param) => {
    let [ key, value ] = param.split('=');
    params[key] = value ? decodeURIComponent(value.replace(/\+/g, ' ')) : '';

    return params;
  }, {});
}

export function ensureHttp(url) {
  if (!url.startsWith('http')) {
    return 'http://' + url;
  }
}

/**
 * Get the page type from a url.
 * e.g. /cl/27/Baerbara-Datorer -> cl
 */
export function getPageType(url) {
  if (!url) {
    return;
  }

  return url.split('/')[1];
}

export function removeHashFromUri(uri) {
  if (uri.includes('?')) {
    return uri.replace('#', '&');
  } else {
    return uri.replace('#', '?');
  }
}
