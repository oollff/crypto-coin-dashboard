import { Router } from 'express';
import InMemoryCache from '../../inMemoryCache';
import { fetchPrice } from './coinPriceUtils';

const coinPrice = Router();

const baseUrl = 'https://www.reddit.com/r';

const baseCache = '/api/coinPrice_';
const ttl = 60 * 60 * 0.1; // cache for 6min
const cache = new InMemoryCache(ttl); // Create a new cache service instance

coinPrice.get('/getCoinPrice/:baseSymbol/:askSymbol/:exchange', (req, res) => {
  console.log('getCoinPrice');

  const baseSymbol = req.params.baseSymbol;
  const askSymbol = req.params.askSymbol;
  const exchange = req.params.exchange;

  const key = `${baseCache}getCoinPrice${askSymbol}/${baseSymbol}/${exchange}`;

  cache.get(key, () => fetchPrice(baseSymbol, askSymbol, exchange)).then((result) => {
    if (result) {
      res.send(result);
    } else {
      res.send({});
    }
  }).catch(err => {
    res.status(500).send(`Cannot get price for pair: ${baseSymbol} - ${askSymbol} at ${exchange}`);
  });
});

coinPrice.get('/flush/cache', (req, res) => {
  cache.delStartWith(baseCache);
  res.status(200).send('flushed the cache');
});

export default coinPrice;
