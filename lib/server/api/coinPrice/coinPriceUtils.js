import axios from 'axios';

const bittrexBase = 'https://bittrex.com/api/v1.1/public/getmarketsummary?market=';

class Price {
  constructor(symbol, high, low, volume, last, prevDay, priceChange) {
    this.symbol = symbol;
    this.high = high;
    this.low = low;
    this.Volume = volume;
    this.last = last;
    this.prevDay = prevDay;
    this.priceChange = priceChange;
  }

  get object() {
    return {
      symbol: this.symbol,
      high: this.high,
      low: this.low,
      volume: this.Volume,
      last: this.last,
      prevDay: this.prevDay,
      priceChange: this.priceChange,
    };
  }
}

function fetchPrice(baseSymbol, coinSymbol, exchange) {

  switch (exchange) {
    case 'BITTREX':
      return axios.get(`${bittrexBase}${baseSymbol}-${coinSymbol}`).then((res) => {
        if (res && res.data && res.data.success === true) {
          const result = res.data.result[0];

          const priceChange = (result.Last - result.PrevDay) / result.PrevDay * 100;

          return new Price(
            coinSymbol, result.High, result.Low, result.Volume, result.Last, result.PrevDay, priceChange
          ).object;
        }

        return '';
      });
    default:
      return;

  }
}

module.exports = {
  fetchPrice,
};
