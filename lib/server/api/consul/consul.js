import { Router } from 'express';
import Consul from 'consul';

const consulClient = Consul({
  host: 'localhost',
  port: '8500'
});

const consul = Router();

// Delete key from consul
consul.get('/consul/deleteKey/', (req, res) => {
  consulClient.kv.del({ key: req.query.keyName }, (err,) => {
    if (err) {
      throw err;
    } else {
      res.send('OK');
    }
  });
});

// Get all consul keys starting from key
consul.get('/consul/getKeys/*', (req, res) => {
  consulClient.kv.get({ recurse: true }, (err, keys) => {
    if (err) {
      throw err;
    }

    if (keys) {
      const map1 = keys.map((key) => {
        key.Value = JSON.parse(key.Value);

        return key;
      });

      res.send(map1);
    } else {
      // There are no Keys
      res.send([]);
    }

  });
});

// Get all consul keys starting from key
consul.get('/consul/getConsulKey/', (req, res) => {
  consulClient.kv.get({ key: req.query.keyName }, (err, key) => {
    if (err) {
      throw err;
    }
    if (key) {
      key.Value = JSON.parse(key.Value);
      res.send(key);
    } else {
      // There is no Key
      res.send('');
    }
  });
});

// Get all consul keys starting from key
consul.put('/consul/postConsulKey', (req, res) => {
  // Json can only store a stringified json object
  console.log('/consul/postConsulKey');
  const value = JSON.stringify(req.body.key.Value);

  consulClient.kv.set({ key: req.body.key.Key, value: value }, (err, result) => {
    if (err) {
      throw err;
    }
    if (result) {
      res.send(result);
    }
  });
});

export default consul;
