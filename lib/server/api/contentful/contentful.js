import { Router } from 'express';
import { createClient } from 'contentful';
import { cleanGetAllCoinIds } from './contentfulUtils';
import InMemoryCache from '../../inMemoryCache';

const contentfulClient = createClient({
  // This is the space ID. A space is like a project folder in Contentful terms
  space: 'hx7bu0a3b5k1',
  // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
  accessToken: 'aba8ba16e54d79a9e88a91758365daca281eea9887c4b04585109a4ec7c9630a'
});

const contentful = Router();

const baseCache = '/api/contentful_';
const ttl = 60 * 60 * 2; // cache for 120min
const cache = new InMemoryCache(ttl); // Create a new cache service instance

contentful.get('/contentful/getCoin/:symbol', (req, res) => {
  const key = `${baseCache}getCoin_${req.params.symbol}`;

  const entryQuery = { content_type: 'coin', 'fields.symbol': req.params.symbol };

  cache.get(key, () => contentfulClient.getEntries(entryQuery)).then((result) => {
    if (result && result.items.length > 0 && result.items[0].fields) {
      res.send(result.items[0].fields);
    } else {
      res.send({});
    }
  }).catch(err => {
    res.status(500).send('Cannot get coin');
  });
});

contentful.get('/contentful/getContentfulData', (req, res) => {
  const key = `${baseCache}getContentfulData`;

  cache.get(key, contentfulClient.getEntries).then((result) => {
    res.send(result);
  }).catch(err => {
    res.status(500).send('Cannot get coins');
  });
});

// Get a specific field from a coin model
contentful.get('/contentful/getCoinField/:symbol/:fieldId', (req, res) => {
  const symbol = req.params.symbol;
  const fieldId = req.params.fieldId;
  const key = `${baseCache}getCoinField_${symbol}/${fieldId}`;

  const entryQuery = { content_type: 'coin', 'fields.symbol': symbol, select: 'fields.' + fieldId };

  cache.get(key, () => contentfulClient.getEntries(entryQuery)).then((result) => {
    if (result && result.items.length > 0 && result.items[0].fields && result.items[0].fields.wallets) {
      res.send(result.items[0].fields.wallets);
    } else {
      res.send({});
    }
  }).catch(err => {
    res.status(500).send('Cannot get coin');
  });
});

contentful.get('/contentful/getAllCoinIds', (req, res) => {
  const key = `${baseCache}getAllCoinIds`;

  cache.get(key, () => cleanGetAllCoinIds(contentfulClient)).then((result) => {
    if (result) {
      res.send(result);
    } else {
      res.send({});
    }
  }).catch(err => {
    res.status(500).send('Cannot get coin');
  });
});

contentful.get('/contentful/flush/cache', (req, res) => {
  cache.delStartWith(baseCache);
  res.status(200).send('flushed the cache');
});

export default contentful;
