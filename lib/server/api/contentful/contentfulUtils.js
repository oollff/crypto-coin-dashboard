
function cleanGetAllCoinIds(contentfulClient) {
  const entryQuery = { content_type: 'coin', select: 'fields.name,fields.symbol' };

  return contentfulClient.getEntries(entryQuery).then(res => {
    return res.items.map(item => {
      return {
        name: item.fields.name,
        symbol: item.fields.symbol,
      };
    });
  });
}

module.exports = {
  cleanGetAllCoinIds,
};
