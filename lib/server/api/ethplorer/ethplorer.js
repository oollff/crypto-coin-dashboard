import { Router } from 'express';
import axios from 'axios';
import InMemoryCache from '../../inMemoryCache';

const ethplorer = Router();

const hostname = 'https://api.ethplorer.io';

const ttl = 60 * 60 * 0.1; // cache for 6min
const cache = new InMemoryCache(ttl); // Create a new cache service instance

ethplorer.get('/getWallet/:name', (req, res) => {
  console.log('ethplorer getWallet: ');

  const walletId = req.params.name;
  const key = `${hostname}_getWallet_${walletId}`;
  const url = `${hostname}/getAddressInfo/${walletId}?apiKey=freekey`;

  cache.get(key, () => axios.get(url)).then((result) => {
    if (result && result.data) {
      res.send(result.data);
    } else {
      res.send({});
    }
  }).catch(err => {
    res.status(500).send(`Cannot get coin ${req.params.name}`);
  });
});

ethplorer.get('/flush/cache', (req, res) => {
  cache.delStartWith(hostname + '_');
  res.status(200).send('flushed the cache');
});

export default ethplorer;
