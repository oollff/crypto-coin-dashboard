import { Router } from 'express';
import InMemoryCache from '../../inMemoryCache';
import { cleanGoogleTrendData } from './googleTrendsApiUtils';
const googleTrends = require('google-trends-api');

// Cache
const baseCache = '/api/googleTrends_';
const ttl = 60 * 60 * 0.2; // cache for 6min
const cache = new InMemoryCache(ttl); // Create a new cache service instance

const googleTrendsApi = Router();

googleTrendsApi.get('/getInterestOverTime/:keyword/:startTime/:endTime', (req, res) => {
  console.log('google getInterestOverTime: ----------');

  const keyword = req.params.keyword.replace(new RegExp('_', 'g'), ' ');
  const startTime = new Date(req.params.startTime.replace(new RegExp('_', 'g'), ' '));
  const endTime = new Date(req.params.endTime.replace(new RegExp('_', 'g'), ' '));

  const params = {
    keyword: keyword,
    startTime: startTime,
    endTime: endTime,
    granularTimeResolution: true,
  };

  const key = `${baseCache}getInterestOverTime${keyword}/${startTime}/${endTime}`;

  cache.get(key, () => googleTrends.interestOverTime(params)
    .then((results) => cleanGoogleTrendData(results)))
    .then((result) => {
      if (result) {
        res.send(result);
      } else {
        res.send({});
      }
    }).catch(err => {
      res.status(500).send(`Cannot get data from google trends: params:${keyword}/${startTime}/${endTime} err:${err}`);
    });
});

googleTrendsApi.get('/flush/cache', (req, res) => {
  cache.delStartWith(baseCache);
  res.status(200).send('flushed the cache');
});

export default googleTrendsApi;
