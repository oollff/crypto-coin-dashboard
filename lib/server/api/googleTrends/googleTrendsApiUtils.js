
function cleanGoogleTrendData(data) {
  // For some reason the data needs to be parsed
  const googleTrendData = JSON.parse(data).default.timelineData.map((timeLineItem) => {
    return {
      formattedTime: timeLineItem.formattedTime.split(',')[0],
      formattedValue: timeLineItem.value,
    };
  });

  return googleTrendData;
}

module.exports = {
  cleanGoogleTrendData,
};
