import { Router } from 'express';
import axios from 'axios';
import InMemoryCache from '../../inMemoryCache';
import { cleanRedditData, cleanRedditPost, cleanRedditComments } from './redditApiUtils';

const redditApi = Router();

const baseUrl = 'https://www.reddit.com/r';

const baseCache = '/api/reddit_';
const ttl = 60 * 60 * 0.1; // cache for 6min
const cache = new InMemoryCache(ttl); // Create a new cache service instance

redditApi.get('/getSubRedditPosts/:subReddit/:tab', (req, res) => {
  console.log('getSubRedditPosts');

  const subReddit = req.params.subReddit;
  const tab = req.params.tab;

  const url = `${baseUrl}/${subReddit}/${tab}.json?limit=5`;

  const key = `${baseCache}getSubRedditPosts_${subReddit}/${tab}`;

  cache.get(key, () => axios.get(url).then(res => {
    const posts = cleanRedditData(res.data);

    const commentsBaseUrl = `${baseUrl}/${subReddit}/comments`;

    return Promise.all(posts.map( post => axios.get(`${commentsBaseUrl}/${post.id}/.json?limit=5`)))
      .then((responseArray) => {
        return responseArray.map(responseArrayItem => {
          const postWithComments = responseArrayItem.data;
          // postWithComments[0] = The Post that the comments belong to
          // postWithComments[1] = Comments to the post in [0]

          return {
            post: cleanRedditPost(postWithComments[0]),
            comments: cleanRedditComments(postWithComments[1]),
          };
        });
      });
  })).then((result) => {
    if (result) {
      res.send(result);
    } else {
      res.send({});
    }
  }).catch(err => {
    res.status(500).send(`Cannot get data for sub reddit: ${subReddit} tab: ${tab}`);
  });
});

redditApi.get('/flush/cache', (req, res) => {
  cache.delStartWith(baseCache);
  res.status(200).send('flushed the cache');
});

export default redditApi;
