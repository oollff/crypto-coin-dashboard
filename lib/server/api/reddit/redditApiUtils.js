const moment = require('moment');
moment.updateLocale('en', {
  relativeTime : {
    future: 'in %s',
    past:   '%s ago',
    s  : '%ds',
    ss : '%ds',
    m:  '%dm',
    mm: '%dm',
    h:  '%dh',
    hh: '%dh',
    d:  '%dd',
    dd: '%dd',
    M:  '%dm',
    MM: '%dm',
    y:  '%dy',
    yy: '%dy'
  }
});

function _getElapsedTime(utcSeconds) {
  const timeElapsed = moment(new Date(0).setUTCSeconds(utcSeconds));

  return timeElapsed.fromNow(true);
}

function cleanRedditData(redditData) {
  return redditData.data.children.map((post) => {
    return {
      id: post.data.id,
    };
  });
}

function cleanReplies(replies) {
  if (!replies || !replies.data || !replies.data.children) {
    return '';
  }

  return replies.data.children.map(reply => {
    return {
      author: reply.data.author,
      body: reply.data.body,
      score: reply.data.score,
      elapsedTime: _getElapsedTime(reply.data.created_utc),
      replies: cleanReplies(reply.data.replies),
    };
  });
}

function cleanRedditPost(post) {
  if (!post || !post.data || !post.data.children) {
    return {};
  }

  return {
    id: post.data.children[0].data.id,
    author: post.data.children[0].data.author,
    title: post.data.children[0].data.title,
    selfText: post.data.children[0].data.selftext,
    score: post.data.children[0].data.score,
    elapsedTime: _getElapsedTime(post.data.children[0].data.created_utc),
    num_comments: post.data.children[0].data.num_comments,
    url: post.data.children[0].data.url,
  };
}

function cleanRedditComments(comment) {
  if (!comment || !comment.data || !comment.data.children) {
    return {};
  }

  return comment.data.children.map(comment => {
    return {
      author: comment.data.author,
      body: comment.data.body,
      score: comment.data.score,
      elapsedTime: _getElapsedTime(comment.data.created_utc),
      replies: cleanReplies(comment.data.replies),
    };
  });
}

module.exports = {
  cleanRedditData,
  cleanRedditPost,
  cleanRedditComments,
};
