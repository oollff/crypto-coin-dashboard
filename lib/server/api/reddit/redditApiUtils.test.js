import { cleanRedditPost, cleanRedditComments } from './redditApiUtils';
import { redditMockPost } from './redditMockData';

Date.now = jest.fn(() => new Date('2018-07-12T04:30:00'));

describe('redditApiUtils', () => {

  describe('cleanRedditPost', () => {
    it('Filter away some post object attributes', () => {
      const cleanedPost = cleanRedditPost(redditMockPost[0]);
      const expectedPost = {
        id: '8yxp64',
        author: 'Hairstylist001',
        elapsedTime: '3d',
        selfText: 'why is tenx missing the deadlines?\n\ni still dont have my tenx card !',
        title: 'is this project dead ?',
        num_comments: 17,
        score: 11,
        url: 'https://www.reddit.com/r/TenX/comments/8yxp64/is_this_project_dead/',
      };
      expect(cleanedPost).toEqual(expectedPost);
    });
  });

  describe('cleanRedditComments', () => {
    it('Filter away some post object attributes', () => {
      const cleanedComments = cleanRedditComments(redditMockPost[1]);

      const expectedComments = [ /* eslint-disable */
        { author: 'jlkc1992', body: 'Project not but token yes', elapsedTime: '3d', replies: '',
          score: 10,
        },
        { author: 'frodev', body: "I gave up, got me a wirex card instead. \n\nAnd this&gt; https://exitscam.me/65", elapsedTime: '3d', replies: '',
          score: 2,
        },
        { author: 'duluoz1', body: "It's not dead, but they have totally changed what we understand the project to be. Pretty scummy and fraudulent in my view.", elapsedTime: '3d',
          replies: [
            {
              'author': 'lateralspin',
              'body': 'Although I agree with this, the reason that TenX had to steer off course, was that it acts as a fiat gateway, therefore it is under the regulations of the state, and the U.S. SEC',
              elapsedTime: "3d",
              'replies': [
                {
                  'author': 'duluoz1',
                  'body': 'I know. At that point they should have wound up operations and refunded investors.',
                  elapsedTime: "3d",
                  'replies': [
                    {
                      'author': 'murghph',
                      'body': "Our just roll with it and get the things the SEC wants done done. Work within regulations to create a product that eventually will change the way people spend crypto.\nStupid Julian and co insisting they don't want it to be a security",
                      elapsedTime: "3d",
                      'replies': [
                        {
                          'author': undefined,
                          'body': undefined,
                          'elapsedTime': "Invalid date",
                          'replies': '',
                          'score': undefined,
                        },
                      ],
                      score: 6,
                    },
                  ],
                  score: 8,
                },
                {
                  'author': undefined,
                  'body': undefined,
                  'elapsedTime': "Invalid date",
                  'replies': '',
                  score: undefined,
                },
              ],
              score: 6,
            },
            {
              'author': undefined,
              'body': undefined,
              'elapsedTime': "Invalid date",
              'replies': '',
              score: undefined,
            },
          ],
          score: 7,
        },
        { author: undefined, body: undefined, elapsedTime: "Invalid date", replies: '',
          score: undefined,
        },
      ]; /* eslint-enable */

      expect(cleanedComments).toEqual(expectedComments);
    });
  });
});
