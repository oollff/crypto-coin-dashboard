import { Router } from 'express';
import Twitter from 'twitter';
import InMemoryCache from '../../inMemoryCache';
import { cleanTweets } from './twitterApiUtils';

const twitterClient = new Twitter({
  consumer_key: 'CRLwXVrM8TeV7XySUr6fcadaI',
  consumer_secret: '0hKwnLFgiBuXvq9F33HAYUOD3ZAzicBE16DccWB0AI9kHsjcqb',
  access_token_key: '751403097835114496-KzekLL8oWmU4EpnjjxGCVFgH7bUBgC0',
  access_token_secret: 'mk8FGS7Rn3XmCrHShd95hUXm04ADdIlT0lRqPxJmLyJt6'
});

// Cache
const baseCache = '/api/twitter_';
const ttl = 60 * 60 * 0.1; // cache for 6min
const cache = new InMemoryCache(ttl); // Create a new cache service instance

const twitterApi = Router();

twitterApi.get('/getTwitterTimeline/:screenName', (req, res) => {
  console.log('twitter getTwitterTimeline: ');

  const screenName = req.params.screenName;
  const params = { screen_name: screenName, count: 5, tweet_mode: 'extended' };

  const key = `${baseCache}getTwitterTimeline${screenName}`;

  const timeLine = cache.getCache(key);
  if (timeLine) {
    res.send(timeLine);
  } else {
    twitterClient.get('statuses/user_timeline', params, function(error, tweets, response) {
      if (!error && tweets) {
        const cleanedTweets = cleanTweets(tweets);
        cache.setToCache(key, cleanedTweets);
        res.send(cleanedTweets);
      } else {
        res.status(500).send(`Cannot get timeline for screenName: ${screenName}`);
      }
    });
  }
});

twitterApi.get('/flush/cache', (req, res) => {
  cache.delStartWith(baseCache);
  res.status(200).send('flushed the cache');
});

export default twitterApi;
