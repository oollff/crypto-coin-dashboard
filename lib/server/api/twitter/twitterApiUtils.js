
function cleanTweets(tweets) {
  return tweets.map((tweet) => {
    tweet = tweet.retweeted_status || tweet;

    const tweetUser = tweet.user;

    return {
      id_str: tweet.id_str,
      created_at: tweet.created_at,
      full_text: tweet.full_text,
      screen_name: tweetUser && tweetUser.screen_name,
      name: tweetUser && tweetUser.name,
      profile_image_url_https: tweetUser && tweetUser.profile_image_url_https,
    };
  });
}

module.exports = {
  cleanTweets,
};
