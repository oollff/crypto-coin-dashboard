import { Router } from 'express';
import axios from 'axios';
import InMemoryCache from '../../inMemoryCache';
import { cleanYoutubeData } from './youtubeApiUtils';

const youtubeApi = Router();

const activitiesApi = 'https://www.googleapis.com/youtube/v3/activities';
const apiKey = 'key=AIzaSyAo6oTUhS8il0OxhPzvc67Nd7iS1O7VWYM';

const baseCache = 'youtube_';
const ttl = 60 * 60 * 0.1; // cache for 6min
const cache = new InMemoryCache(ttl); // Create a new cache service instance

youtubeApi.get('/getChannel/:channelId', (req, res) => {
  console.log('youtube getWallet: ');

  const parts = 'part=snippet,contentDetails';
  const channelId = `channelId=${req.params.channelId}`;

  const url = `${activitiesApi}?${parts}&${channelId}&${apiKey}&maxResults=5`;

  const key = `${baseCache}getChannel_${channelId}`;

  cache.get(key, () => axios.get(url).then(res => cleanYoutubeData(res.data))).then((result) => {
    if (result) {
      res.send(result);
    } else {
      res.send({});
    }
  }).catch(err => {
    res.status(500).send(`Cannot get data for channel: ${req.params.channelId}`);
  });
});

youtubeApi.get('/flush/cache', (req, res) => {
  cache.delStartWith(baseCache);
  res.status(200).send('flushed the cache');
});

export default youtubeApi;
