
function cleanYoutubeData(data) {
  return data.items.map((youtubeItem) => {
    return {
      videoId: youtubeItem.contentDetails.upload.videoId,
      publishedAt: youtubeItem.snippet.publishedAt,
      title: youtubeItem.snippet.title,
      description: youtubeItem.snippet.description,
      thumbnails: youtubeItem.snippet.thumbnails,
      channelTitle: youtubeItem.snippet.channelTitle,
    };
  });
}

module.exports = {
  cleanYoutubeData,
};
