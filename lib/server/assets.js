import { flushFiles } from 'webpack-flush-chunks';

const jsReg = /\.js(\?v=\w*)*$/;
const cssReg = /\.css(\?v=\w*)*$/;

export function getAssets(stats, chunkNames) {
  return {
    scripts: getAssetsOfType('js', stats, chunkNames),
    stylesheets: getAssetsOfType('css', stats, chunkNames),
    cssHash: `<script>window.__CSS_CHUNKS__ = ${JSON.stringify(getCssHash(stats))}</script>`,
  };
}

function getCssHash(stats) {
  let cssHash = {};
  Object.keys(stats.assetsByChunkName)
    .forEach(chunkName => {
      const filteredAssets = getAssetsForKey(chunkName, stats, cssReg);
      if (filteredAssets.length > 0) {
        cssHash[chunkName] = stats.publicPath + filteredAssets[0];
      }
    });

  return cssHash;
}

function getAssetsOfType(type, stats, chunkNames) {
  const reg = type === 'js' ? jsReg : cssReg;
  const manifestAssets = getAssetsForKey('manifest', stats, reg);
  const commonAssets = getAssetsForKey('pr-common-main', stats, reg);
  const chunkAssets = flushFiles(stats, { chunkNames, filter: reg });
  const entryAssets = getEntrypointAssets(stats, reg);

  if (type === 'js') {
    return manifestAssets
      .concat(commonAssets)
      .concat(chunkAssets)
      .concat(entryAssets);
  }
  else {
    return manifestAssets
      .concat(entryAssets)
      .concat(chunkAssets)
      .concat(commonAssets);
  }
}

function getEntrypointAssets(stats, reg) {
  return Object.keys(stats.entrypoints)
    .map(key => getAssetsForKey(key, stats, reg))
    .reduce((a, b) => {
      return a.concat(b);
    }, [] );
}

function getAssetsForKey(key, stats, reg) {
  let assets = [];
  if (stats.assetsByChunkName && stats.assetsByChunkName[key]) {
    const files = stats.assetsByChunkName[key];
    // files could be either string or array, but we always want to return an array.
    if (typeof(files) === 'string') {
      assets = reg.test(files) ? [files] : [];
    }
    else if (files.filter) {
      assets = files.filter(asset => reg.test(asset));
    }
  }

  return assets;
}
