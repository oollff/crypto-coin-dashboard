const environment = require('./environment');
const morgan = require('morgan');

const logFormat = process.env.NODE_ENV === 'development' ? 'dev' : function (tokens, req, res) {
  const xRequestToken = req.headers && req.headers['x-request-token'];
  const xForwardedHost = req.headers && req.headers['x-forwarded-host'];
  const hostname = tokens.hostname && tokens.hostname(req, res);

  return [
    new Date().toUTCString(),
    'method=' + tokens.method(req, res),
    'url=' + tokens.url(req, res),
    'status=' + tokens.status(req, res),
    'content_length=' + tokens.res(req, res, 'content-length'),
    'response_time=' + tokens['response-time'](req, res) + ' ms',
    'hostname=' + hostname,
    'x_forwarded_host=' + xForwardedHost,
    'env=' + environment.NOMAD_JOB_NAME,
    xRequestToken && (`x-request-token=${xRequestToken}`),
  ].join('\t');
};

const useMorganLogging = (server) => {
  server.use(morgan(logFormat));
};

const errorHandler = (err, req, res) => {
  if (err instanceof URIError) {
    res.writeHead(301, {
      Location: encodeURI(unescape(req.url)),
    });
    res.end();

    return;
  }
  console.error(new Date().toUTCString(), err.stack);
  res.status(500).sendFile(path.join(`${__dirname}/static/500-${req.CONTENT_LANGUAGE_CODE}.html`));
};

module.exports = {
  errorHandler,
  useMorganLogging
};
