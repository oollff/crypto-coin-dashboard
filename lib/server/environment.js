const environment = {
  NODE_PORT: process.env.PORT || '4006',
  DATACENTER: process.env.DATACENTER || 'thule',
  CONSUL_FOLDER_ENVIRONMENT: process.env.CONSUL_FOLDER_ENVIRONMENT || 'dev',
  CONSUL_HOST: 'localhost',
  CONSUL_PORT: '8500',
  CONTAINER_TAG: process.env.CONTAINER_TAG || 'stage',
  NOMAD_JOB_NAME: process.env.NOMAD_JOB_NAME || 'dev',
  NODE_ENV: process.env.NODE_ENV,
};

module.exports = environment;
