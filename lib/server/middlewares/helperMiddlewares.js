
function ifNot(paths, middleware) {
  return function (req, res, next) {
    const parts = req.path.split('/');
    if (parts.length > 0 && paths.includes(parts[1])) {
      return next();
    } else {
      return middleware(req, res, next);
    }
  };
}

module.exports = {
  ifNot
};
