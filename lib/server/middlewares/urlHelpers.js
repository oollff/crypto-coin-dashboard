
function getRequestHostname(req) {
  return (req.headers['x-forwarded-host'] && req.headers['x-forwarded-host'].includes(':') ?
    req.headers['x-forwarded-host'].split(':')[0] : req.headers['x-forwarded-host']) ||
        req.hostname;
}

function getCountryCodeWithHostname(hostname) {
  if (hostname.includes('www')) {
    return hostname && hostname.split('.').pop();
  }

  // in development the country code is first in the hostname
  return hostname && hostname.split('.')[0];
}

function getCountryLanguageCode(countryCode) {
  const countryNames = {
    se: 'sv',
    uk: 'en',
    dk: 'da',
  };

  return countryNames[countryCode];
}

function getRequestProtocol(req) {
  return req.headers['x-forwarded-proto'] ? req.headers['x-forwarded-proto'] : 'http';
}

module.exports = {
  getRequestHostname,
  getCountryCodeWithHostname,
  getCountryLanguageCode,
  getRequestProtocol,
};
