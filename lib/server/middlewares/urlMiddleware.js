const {
  getRequestHostname,
  getCountryCodeWithHostname,
  getCountryLanguageCode,
  getRequestProtocol,
} = require('./urlHelpers');

function stripTrailingChar(char) {
  return (req, res, next) => {
    const url = req.originalUrl;
    const reg = new RegExp(char + '$');

    // For the homepage the originalUrl is / and since we can't
    // redirect to an empty url we just move on.
    if (url === '/') {
      return next();
    }
    else if (url.match(reg)) {
      const newUrl = url.replace(reg, '');

      return res.redirect(301, newUrl);
    }

    return next();
  };
}

function getLocale() {
  return (req, res, next) => {
    const apiHostname = getRequestHostname(req);
    const baseUrl = apiHostname.split('/')[0];
    const countryCode = getCountryCodeWithHostname(baseUrl);

    req.API_HOSTNAME = apiHostname;
    req.API_COUNTRY_CODE = countryCode;
    req.CONTENT_LANGUAGE_CODE = getCountryLanguageCode(countryCode);
    req.REQ_PROTOCOL = getRequestProtocol(req);
    next();
  };
}

module.exports = {
  stripTrailingChar,
  getLocale,
};
