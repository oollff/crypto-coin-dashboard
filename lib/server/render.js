import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { Provider } from 'react-redux';
import Helmet from 'react-helmet';
import { StaticRouter } from 'react-router-dom';
import createHistory from 'history/createMemoryHistory';
import { CookiesProvider, Cookies } from 'react-cookie';
import MobileDetect from 'mobile-detect';
import { v1 } from 'uuid';
import { flushChunkNames } from 'react-universal-component/server';
import { setServerRequestInfo, setXRequestToken } from '../app/shared/reduxStore/requestState';
import configureStore from '../app/redux/configureStore';
import AppContainer from '../app/AppContainer';
import environment from './environment';
import windowConfig from './windowConfig';
import { getStaticRoutes, error, preloadData, setRouteToRedux } from './renderUtils';
import { getAssets } from './assets';

// Create node instance variable which will only get populated one time (during server start)
const staticRoutes = getStaticRoutes();

const render = ({ clientStats }) => (req, res) => {
  const history = createHistory();
  const store = configureStore(history);
  const baseUrl = req.API_HOSTNAME.split('/')[0];
  const universalCookies = new Cookies(req.universalCookies).cookies;

  // Add a request token to our redux store. It is used as a header in all our consul requests (for tracking purposes).
  store.dispatch(setXRequestToken({
    xRequestToken: v1()
  }));

  setRouteToRedux(store, req);

  // Populate store with request info
  saveRequestInfo();

  // Fetch data that will be used server side then render our index.eje
  preloadData(store, req, staticRoutes).then(() => {
    // Populate store with request info
    saveRequestInfo();

    // Setup React-Router server-side rendering
    // staticContext is populated in AppContainer > route > render props.staticContext
    const staticContext = {};
    const htmlContent = getHtmlContent(staticContext);

    // Get the styles and scripts used to render this route
    const chunkNames = flushChunkNames();
    const { scripts, stylesheets, cssHash } = getAssets(clientStats, chunkNames);

    // Check if the render result contains a redirect, if so we need to set
    // the specific status and redirect header in the end response
    if (staticContext.url) {
      res.writeHead(301, {
        Location: encodeURI(staticContext.url),
      });
      res.end();
    } else {
      const status = staticContext.status || 200;
      const storeState = store.getState();
      const pathPrefix = environment.CONTAINER_TAG === 'prod' ? windowConfig.WEBAPP_CDN : '';
      const publicPath = `${pathPrefix}${clientStats.publicPath}`; // Revert until redirect issue is fixed by devops
      // Render index.ejs template with pre loaded data
      res.status(status).render(
        'index',
        {
          clientConfig: escape(JSON.stringify(windowConfig)),
          environmentConfig: escape(JSON.stringify(environment)),
          head: Helmet.renderStatic(),
          htmlContent,
          initialState: escape(JSON.stringify(storeState)),
          countryCode: req.API_COUNTRY_CODE,
          scriptSrcs: scripts,
          styleSrc: stylesheets,
          styleCount: stylesheets.length,
          publicPath,
          cssHash,
          languageCode: req.CONTENT_LANGUAGE_CODE,
        }
      );
    }
  })
    .catch((err) => {
      error(err, res, req);
    });

  function saveRequestInfo() {
    let mobileDetect = new MobileDetect(req.headers['user-agent']);
    const reqIp = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const officeIpsArray = [
      '94.254.17.126', // Stockholm office
      '87.54.13.94', // DK office
      '87.54.13.89', // DK office
    ];
    const clientHasOfficeIp = officeIpsArray.indexOf(reqIp) > -1;

    store.dispatch(setServerRequestInfo({
      requestProtocol: req.REQ_PROTOCOL,
      requestHostname: req.API_HOSTNAME,
      host: req.hostname,
      baseUrl,
      countryCode: req.API_COUNTRY_CODE,
      isMobile: !!mobileDetect.mobile() && !mobileDetect.tablet(),
      isTablet: !!mobileDetect.tablet(),
      initialUrl: req.url,
      reqIp,
      clientHasOfficeIp,
    }));
  }

  function getHtmlContent(context) {
    const renderToString = ReactDOMServer.renderToString(
      <CookiesProvider cookies={universalCookies}>
        <Provider store={store}>
          <StaticRouter location={req.url} context={context}>
            <AppContainer />
          </StaticRouter>
        </Provider>
      </CookiesProvider>
    );

    return renderToString;
  }
};

export default render;
