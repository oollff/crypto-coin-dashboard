import path from 'path';
import url from 'url';
import Promise from 'bluebird';
import { matchRoutes } from 'react-router-config';
import { setFetchedContainerName } from '../app/shared/reduxStore/requestState';
import { loadConsulSettings } from '../app/shared/reduxStore/siteSettingsState';
import { setInitialRoute } from '../app/shared/reduxStore/routerState';
import routes from '../app/routes/routes';

// The components that we're declaring in routes.js are imported async, so we can't garantee that
// they're loaded when we want to call fetchData on the server and thus we need to import them
// staticly/synchronously. This we do with an old-school require.
function getStaticRoutes() {
  return routes.map(route => {
    const staticComponent = require('../app/' + route.componentPath).default;

    if (route.routes) {
      let staticChildren = route.routes.map((childRoute) => {
        const staticComponent = require('../app/' + childRoute.componentPath).default;

        return {
          staticComponent,
          ...childRoute
        };
      });
      route.routes = staticChildren;
    }

    return {
      staticComponent,
      ...route
    };
  });
}

const error = (err, res, req) => {
  if (err.code === 'ECONNABORTED') {
    res.status(503)
      .sendFile(path.join(`${__dirname}/static/500-${req.CONTENT_LANGUAGE_CODE}.html`));
    console.error(`${new Date().toUTCString()}\t==> Request timeout= ${err}\turl=${req.url}`);
  }
  else if (err.response && err.response.status === 404) {
    res.writeHead(302, {
      Location: '/error' + encodeURI(req.url),
    });
    console.error(`${new Date().toUTCString()}\t==> Page not found= ${err}\turl=${req.url}`);
    res.end();
  }
  else if (err.response && err.response.status === 301) {
    const redirectUrl = err.response.headers.location.replace('http:///', '/');
    res.writeHead(301, {
      Location: encodeURI(redirectUrl),
    });
    console.log(`${new Date().toUTCString()}\t==> Redirect to Location=${redirectUrl}`);
    res.end();
  }
  else {
    const status = err.response ? err.response.status : 502;
    res.status(status)
      .sendFile(path.join(`${__dirname}/static/500-${req.CONTENT_LANGUAGE_CODE}.html`));
    console.error(`${new Date().toUTCString()}\t==> Rendering routes error= ${err}\turl=${req.url}`);
    console.error(err.stack);
  }
};

function preloadData(store, req, staticRoutes) {
  store.dispatch(loadConsulSettings(req.consulSettings));

  return Promise.all(
    getRouteDataRequests(store, req, staticRoutes)
  );
}

// Load data on server-side
function getRouteDataRequests(store, req, staticRoutes) {
  // Get the routes that matches req.url
  const matchedRoutes = matchRoutes(staticRoutes, req.url);

  // Collect all fetchData promises from all match routes. This allows child routes to have a fetchData as well
  const matchedRoutesPromises =  matchedRoutes
  // No need to process routes without fetchData()
    .filter(({ route }) => {
      // All modules are wrapped by the Connect HOC, thus located in
      // Connect.WrappedComponent
      // e.g. <Connect(PRContainer)><PRContainer /><Connect(PRContainer)/>
      const wrappedContainer = route.staticComponent.WrappedComponent;

      return wrappedContainer && !!wrappedContainer.fetchData;
    })
    .map(({ route, match }) => {
      const { params } = match;
      params.apiHostname = req.API_HOSTNAME;
      params.location = url.parse(match.url);

      const wrappedContainer = route.staticComponent.WrappedComponent;

      const routePromises = [
        store.dispatch(setFetchedContainerName(wrappedContainer.name)),
        wrappedContainer.fetchData(store, params)
      ];

      return Promise.all(routePromises);
    });

  return matchedRoutesPromises;
}

function setRouteToRedux(store, req) {
  const parsedUrl = url.parse(req.url);
  store.dispatch(setInitialRoute({
    hash: parsedUrl.hash,
    pathname : parsedUrl.pathname,
    search : parsedUrl.search,
    state: undefined,
    key: 'server',
  }));
}

module.exports = {
  error,
  preloadData,
  setRouteToRedux,
  getStaticRoutes,
};
