const Express = require('express');
const webpack = require('webpack');
const path = require('path');
const webpackDevMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const webpackHotServerMiddleware = require('webpack-hot-server-middleware');
const clientConfig = require('../../webpack/client.dev');
const serverConfig = require('../../webpack/server.dev');
const serverMiddlewareConfig = require('../../webpack/server.middleware');
const environment = require('./environment');
const { errorHandler } = require('./constants');
const { setupApi, devAndProdSetup } = require('./serverUtils');

const publicPath = clientConfig.output.publicPath;

// ---------------------------------------------- //

let server = new Express();

let isBuilt = false;
// Start the server once all webpack is done
function done() {
  if (!isBuilt) {
    server.listen(environment.NODE_PORT, () => {
      isBuilt = true;
      console.log(`${new Date().toUTCString()} Node server is listening to port: ${environment.NODE_PORT}`);
    });
  }
}

const res = p => path.resolve(__dirname, p);

devAndProdSetup(server, path, res);

console.log('Compiling with Webpack...');
webpack([serverMiddlewareConfig]).run((err, stats) => {
  if (!err) {
    const middlewareAssets = stats.toJson().children[0].assetsByChunkName;
    setUpMiddlewares(middlewareAssets);

    const compiler = webpack([clientConfig, serverConfig]);
    const clientCompiler = compiler.compilers[0];
    const options = {
      publicPath,
      stats: { colors: true },
      serverSideRender: true
    };
    server.use(webpackDevMiddleware(compiler, options));
    server.use(webpackHotMiddleware(clientCompiler));
    server.use(webpackHotServerMiddleware(compiler));

    server.use((req, res) => {
      res.send(res.locals.webpackStats.toJson());
    });

    server.use(errorHandler);

    compiler.plugin('done', done);
  }
  else {
    console.error('Something went wrong', err);
  }
});

function setUpMiddlewares(middlewareAssets) {
  setupApi(server, serverConfig.output.path, middlewareAssets);
}
