const path = require('path');
const Express = require('express');
const compression = require('compression');
const environment = require('./environment');
const { errorHandler } = require('./constants');
const { setupApi, devAndProdSetup } = require('./serverUtils');

const res = p => path.resolve(__dirname, p);

// ---------------------------------------------- //

let server = new Express();
server.use(compression());

devAndProdSetup(server, path, res);

const serverOutputPath = res('../server');
const clientStats = require('../stats.client.json');
const render = require(`${serverOutputPath}/render.js`).default;

const clientOutputPath = res('../client');

setUpMiddlewares();

server.use('/webapp-assets/', Express.static(clientOutputPath, { maxAge: '360d' }));
server.use(render({ clientStats }));
server.use(errorHandler);

server.listen(environment.NODE_PORT, () => {
  console.log(`${new Date().toUTCString()} Node server is listening to port: ${environment.NODE_PORT}`);
});

function setUpMiddlewares() {
  const serverOutputPath = res('../server');
  const middlewareAssets = require(`${serverOutputPath}/../stats.middleware.json`).assetsByChunkName;
  setupApi(server, serverOutputPath, middlewareAssets);
}
