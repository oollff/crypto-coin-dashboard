const Express = require('express');
const { stripTrailingChar, getLocale } = require('./middlewares/urlMiddleware');
const { ifNot } = require('./middlewares/helperMiddlewares');
const { useMorganLogging } = require('./constants');

function setupApi(server, serverOutputPath, middlewareAssets) {
  server.use('/api', require(`${serverOutputPath}/${middlewareAssets.contentful}`).default);
  server.use('/api/ethplorer', require(`${serverOutputPath}/${middlewareAssets.ethplorer}`).default);
  server.use('/api/youtube', require(`${serverOutputPath}/${middlewareAssets.youtube}`).default);
  server.use('/api/twitter', require(`${serverOutputPath}/${middlewareAssets.twitterApi}`).default);
  server.use('/api/googleTrends', require(`${serverOutputPath}/${middlewareAssets.googleTrends}`).default);
  server.use('/api/reddit', require(`${serverOutputPath}/${middlewareAssets.reddit}`).default);
  server.use('/api/coinPrice', require(`${serverOutputPath}/${middlewareAssets.coinPrice}`).default);
}

function devAndProdSetup(server, path, res) {
  const bodyParser = require('body-parser');
  const cookiesMiddleware = require('universal-cookie-express');

  // to support JSON-encoded bodies
  server.use(bodyParser.json());

  useMorganLogging(server);

  server.use(cookiesMiddleware());

  server.set('views', path.join(__dirname, 'views'));
  server.set('view engine', 'ejs');

  // Avoid running everything twice if a favicon is requested:
  server.get('/favicon.ico', (req, res) => {
    res.status(404).send('Not found');
  });

  // Add google verification file
  server.get('/googleSomeVerificationId.html', function(req, res) {
    res.sendFile(path.join(__dirname, 'static', 'google2e3a53348af68b8a.html'));
  });

  server.get('/manifest.json', function(req, res) {
    res.sendFile(path.join(__dirname, 'static', 'manifest.json'));
  });

  server.use(getLocale());
  server.use(ifNot(['api', 'consul'], stripTrailingChar('/')));

  server.use('/fonts/', Express.static(res('./static/fonts'), { maxAge: '360d' }));
}

module.exports = {
  setupApi,
  devAndProdSetup,
};
