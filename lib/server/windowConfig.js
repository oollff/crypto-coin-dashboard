/*
 * General config object set on server side and given to client though window object.
 * Important: do NOT put sensitive information here as the client side has access.
 * window.__config__
 */

const config = {
  // Allow ('YES') or disallow ('NO') the logger middleware set in app/store/configureStore.js
  ALLOW_LOGGER_MIDDLEWARE: 'NO',

  // Built asset folder name, important: it's also hard coded in gulp and webpack file
  BUILT_ASSETS_FOLDER: 'webapp-assets',

  // PriceRunner CDN base URL, without trailing slash
  WEBAPP_CDN: 'https://images.webappcdn.com',
};

module.exports = config;
