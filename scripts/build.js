const shell = require('shelljs');

shell.echo('Cleaning up old files...');
shell.exec('rimraf dist/');
shell.mkdir('dist');
shell.echo('\nCompiling...');
shell.echo('[1/3]\twebpack/server.prod.js');
shell.exec('webpack --config webpack/server.prod.js --json > dist/stats.server.json');

shell.echo('[2/3]\twebpack/server.middleware.js');
shell.exec('webpack --config webpack/server.middleware.js --json > dist/stats.middleware.json');

shell.echo('[3/3]\twebpack/client.prod.js');
shell.exec('webpack --config webpack/client.prod.js --json > dist/stats.client.json');

shell.echo('\nCopying files...');
shell.cp('dist/stats.client.json', 'dist/client/stats.client.json');
shell.cp('lib/server/server.js', 'dist/server');
shell.cp('lib/server/serverUtils.js', 'dist/server');
shell.cp('lib/server/windowConfig.js', 'dist/server');
shell.cp('lib/server/constants.js', 'dist/server');
shell.cp('lib/server/environment.js', 'dist/server');
shell.cp('-R', 'lib/server/static', 'dist/server');
shell.cp('-R', 'lib/server/views', 'dist/server');
shell.cp('-R', 'lib/server/middlewares', 'dist/server');

shell.echo('\nDone!');
