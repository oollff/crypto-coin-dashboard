import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

jest.mock('react-redux', () => ({
    connect: () => params => params,
}));

// Necessary for projects using slick
window.matchMedia = window.matchMedia || function() {
    return {
        matches : false,
        addListener : function() {},
        removeListener: function() {}
    };
};

window.__CONFIG__ = require('./lib/server/windowConfig');
window.__ENVIRONMENT__ = require('./lib/server/environment');
