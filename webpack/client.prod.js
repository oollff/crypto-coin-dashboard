const path = require('path');
const webpack = require('webpack');
const ExtractCssChunks = require('extract-css-chunks-webpack-plugin');
const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

const BUILT_ASSETS_FOLDER = '/webapp-assets/';

module.exports = {
  name: 'client',
  target: 'web',
  devtool: false,
  entry: {
    'immutable': [
      'immutable',
    ],
    'moment': [
      'moment',
    ],
    'polyfills': [
      'babel-polyfill',
    ],
    'react': [
      'react-dom',
    ],
    'vendor': [
      'axios',
      'history',
      'react-cookie',
      'react-helmet',
      'react-redux',
      'react-router-dom',
      'react-router-redux',
      'react-router',
      'react-universal-component',
      'redux-thunk',
      'universal-cookie',
    ],
    main: [
      path.resolve(__dirname, '../', 'lib', 'app', 'client'),
    ],
  },
  output: {
    filename: '[name].js?v=[chunkhash]',
    chunkFilename: '[name].js?v=[chunkhash]',
    path: path.join(__dirname, '../', 'dist', 'client'),
    publicPath: BUILT_ASSETS_FOLDER,
  },
  module: {
    rules: [
      {
        test: /(?!.*\.test)\.js$/,
        exclude: [/node_modules/, /__snapshots__/, /__tests__/],
        use: 'babel-loader',
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: ExtractCssChunks.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                modules: true,
                localIdentName: '[hash:base64:10]',
              },
            },
            {
              loader: 'postcss-loader',
            },
            {
              loader: 'sass-loader',
            }
          ]
        })
      }
    ]
  },
  resolve: {
    modules: [
      path.join(__dirname, '../', 'lib', 'app'),
      path.join(__dirname, '../', 'lib', 'server'),
      path.join(__dirname, '../', 'assets'),
      'node_modules'
    ]
  },
  plugins: [
    new ExtractCssChunks({
      filename: '[name].css?v=[contenthash]',
    }),
    new webpack.optimize.CommonsChunkPlugin({
      async: 'pr-common',
      minChunks(module, count) {
        return count >= 3;
      },
    }),
    new webpack.optimize.CommonsChunkPlugin({
      names: [
        'vendor',
        'react',
        'moment',
        'immutable',
        'polyfills',
      ],
      filename: '[name].js?v=[chunkhash]',
      minChunks: Infinity,
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'manifest',
      filename: '[name].js?v=[chunkhash]',
      minChunks: Infinity,
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production'
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
    }),
    new webpack.DefinePlugin({
      'process.env.BROWSER': true,
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        screw_ie8: true,
        warnings: false,
      },
      mangle: {
        screw_ie8: true,
      },
      output: {
        screw_ie8: true,
        comments: false,
      },
      sourceMap: true,
    }),
    new webpack.ContextReplacementPlugin(/moment[\\\/]locale$/, /^\.\/(en-gb|sv|da)$/),
    new LodashModuleReplacementPlugin,
    new webpack.HashedModuleIdsPlugin(), // not needed for strategy to work (just good practice)
  ]
};
