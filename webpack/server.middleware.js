const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const WriteFilePlugin = require('write-file-webpack-plugin');

const res = p => path.resolve(__dirname, p);

const nodeModules = res('../node_modules');
const output = res('../dist/server');

// if you're specifying externals to leave unbundled, you need to tell Webpack
// to still bundle `react-universal-component`, `webpack-flush-chunks` and
// `require-universal-module` so that they know they are running
// within Webpack and can properly make connections to client modules:
const externals = fs
  .readdirSync(nodeModules)
  .filter(x => !/\.bin|react-universal-component|webpack-flush-chunks/.test(x))
  .reduce((externals, mod) => {
    externals[mod] = `commonjs ${mod}`;

    return externals;
  }, {});

externals['react-dom/server'] = 'commonjs react-dom/server';

module.exports = {
  name: 'server',
  target: 'node',
  devtool: 'eval',

  entry: {
    consul: [
      res('../lib/server/api/consul/consul.js')
    ],
    contentful: [
      res('../lib/server/api/contentful/contentful.js')
    ],
    ethplorer: [
      res('../lib/server/api/ethplorer/ethplorer.js')
    ],
    youtube: [
      res('../lib/server/api/youtube/youtube.js')
    ],
    twitterApi: [
      res('../lib/server/api/twitter/twitter.js')
    ],
    googleTrends: [
      res('../lib/server/api/googleTrends/googleTrends.js')
    ],
    reddit: [
      res('../lib/server/api/reddit/reddit.js')
    ],
    coinPrice: [
      res('../lib/server/api/coinPrice/coinPrice.js')
    ],
  },
  externals,
  output: {
    path: output,
    filename: '[name].js',
    libraryTarget: 'commonjs2'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      },
    ]
  },
  resolve: {
    modules: [
      path.join(__dirname, '../', 'lib', 'app'),
      'node_modules'
    ]
  },
  plugins: [
    new WriteFilePlugin(),
    new webpack.optimize.LimitChunkCountPlugin({
      maxChunks: 1
    }),
    new webpack.DefinePlugin({
      'process.env.BROWSER': false,
    })
  ]
};
